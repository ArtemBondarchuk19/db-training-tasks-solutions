use CinemaDb
go

declare @json nvarchar(max) =
N'
  [
    {
      "name": "Online using card"
    },
    {
      "name": "Online using Samsung Pay"
    },
    {
      "name": "Online using Apple Pay"
    },
    {
      "name": "Online using Android Pay"
    }
  ]
'

exec dbo.usp_ParseJsonAddPaymentType
    @json = @json
go