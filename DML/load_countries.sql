use CinemaDb
go

declare @countries nvarchar(max) =
N'
 [
  {
    "name": "Belarus"
  },
  {
    "name": "Ukraine"
  },
  {
    "name": "Russia"
  },
  {
    "name": "Poland"
  },
  {
    "name": "England"
  },
  {
    "name": "Canada"
  },
  {
    "name": "France"
  },
  {
    "name": "Germany"
  },
  {
    "name": "USA"
  }
 ]
'

exec dbo.usp_ParseJsonAddCountry
    @json = @countries
go