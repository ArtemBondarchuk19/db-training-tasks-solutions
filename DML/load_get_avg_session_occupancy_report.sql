use CinemaDb
go

declare @json nvarchar(max) =
'
    {
        "startDate": "2010-07-15 01:00:00",
        "endDate": "2022-07-15 23:00:00",
        "interval": {
            "start": "01:00:00"
        },
        "cities": [
            {
                "name": "Vitebsk",
                "country": {
                    "name": "Belarus"
                }
            },
            {
                "name": "Lviv",
                "country": {
                    "name": "Ukraine"
                }
            },
            {
                "name": "London",
                "country": {
                    "name": "England"
                }
            },
            {
                "name": "Munich",
                "country": {
                    "name": "Germany"
                }
            },
            {
                "name": "Berlin",
                "country": {
                    "name": "Germany"
                }
            }
        ],
        "countries": [
            {
                "name": "Germany"
            },
            {
                "name": "Ukraine"
            }
        ]
    }
'

exec dbo.usp_ParseJsonGetAvgSessionOccupancyReport @json
go