﻿use CinemaDb
go

if object_id('dbo.usp_LoadData', 'P') is not null
    drop procedure dbo.usp_LoadData
go

create proc dbo.usp_LoadData
(
	@table			nvarchar(100),
	@parser			nvarchar(100),
	@schema			nvarchar(100)
)
as
begin
	declare @queryText nvarchar(1000) =
	'
		declare @data nvarchar(max)
		declare table_cursor cursor local forward_only read_only static
		for
		select              [Data]
		from                {schema}.{table}

		open table_cursor
		fetch next from table_cursor into @data
		while @@FETCH_STATUS = 0
		begin
			exec {schema}.{parser} @json = @data
			fetch next from table_cursor into @data
		end
		close table_cursor
		deallocate table_cursor
	'

	set @queryText = replace(@queryText,	'{schema}',		@schema)
	set @queryText = replace(@queryText,	'{table}',		@table)
	set @queryText = replace(@queryText,	'{parser}',		@parser)

	execute(@queryText)
end
go