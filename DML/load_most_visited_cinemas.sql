Use CinemaDb
go

declare @json nvarchar(max) =
N'
 {
  "city": {
    "name": "Minsk",
    "country": {
      "name": "Belarus"
    }
  },
  "count": 5
 }
'

exec dbo.usp_ParseJsonMostVisitedCinemas @json
go