use CinemaDb
go

declare @json nvarchar(max) =
N'
[
	{
		"name": "usual",
		"coefficient": "1.0"
	},
	{
		"name": "VIP",
		"coefficient": "1.5"
	},
	{
		"name": "comfort",
		"coefficient": "1.2"
	},
	{
		"name": "private",
		"coefficient": "2"
	},
	{
		"name": "deluxe",
		"coefficient": "1.7"
	},
	{
		"name": "recliner",
		"coefficient": "2.3"
	},
	{
		"name": "love seat",
		"coefficient": "1.7"
	}
]
'

exec dbo.usp_ParseJsonAddPlaceType
    @json = @json
go