use CinemaDb
go

declare @json nvarchar(max) =
N'
 {
  "startDate": "2020-12-01",
  "endDate": "2022-12-01",
  "countries": [
    {
      "name": "Ukraine"
    },
    {
      "name": "England"
    },
    {
      "name": "USA"
    },
    {
      "name": "France"
    }
  ]
 }
'

exec dbo.usp_ParseJsonMostSaledMovies @json
go