use CinemaDb
go

declare @json nvarchar(max) =
N'
    [
      {
        "name": "2D"
      },
      {
        "name": "3D"
      },
      {
        "name": "5D"
      }
    ]
'

exec dbo.usp_ParseJsonAddDisplayType
    @json = @json
go