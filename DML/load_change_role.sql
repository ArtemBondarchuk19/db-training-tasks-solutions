use CinemaDb
go

declare @json nvarchar(max) = 
N'{
  "user": {
    "email": "test@test.com"
  },
  "users": [
    {
      "email": "test1@test.com"
    },
    {
      "email": "test2@test.com"
    }
  ],
  "role" :{
    "name":"admin" 
  }
}'

exec dbo.usp_ParseJsonChangeRole @json
go

declare @json nvarchar(max) = 
N'{
  "user": {
    "email": "test@test.com"
  },
  "users": [
    {
      "email": "test1@test.com"
    }
  ],
  "role" :{
    "name":"user" 
  }
}'

exec dbo.usp_ParseJsonChangeRole @json
go