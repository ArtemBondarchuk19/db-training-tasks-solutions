Use CinemaDb
go

declare @js nvarchar(max) =
N'
 {
  "cinema": {
    "address": "vgibpulzdkysfcqsvblnwyyhwwvfmoxtbqohs",
    "name": "Imedni",
    "city": {
      "name": "Belfast",
      "country": {
        "name": "England"
      }
    }
  },
  "dateFrom": "2000-07-15",
  "dateTo": "2022-07-15"
 }
'

exec dbo.usp_ParseJsonMostProfitableMovies @js
go