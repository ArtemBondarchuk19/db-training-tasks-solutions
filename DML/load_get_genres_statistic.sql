use CinemaDb;
go

declare @json nvarchar(max) =
N'{
 
  "endDate":"2023",
  "genres": [
    {
        "name":"Action"
    },
    {
        "name":"Drama"
    }
  ],
  "lowerBound":5000
}'
exec usp_GetGenresStatistic @json
go