use CinemaDb
go

declare @js nvarchar(max) = 
N'[
  {
    "name": "user"
  },
  {
    "name": "admin"
  }
]'

exec dbo.usp_ParseJsonAddRole @js
go