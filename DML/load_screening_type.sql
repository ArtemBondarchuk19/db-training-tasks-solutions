use CinemaDb
go

declare @js nvarchar(max) =
N'[
  {
    "name": "morning",
    "start": "06:00:00",
    "end": "11:59:59",
    "coefficient": 0.90
  },
  {
    "name": "daytime",
    "start": "12:00:00",
    "end": "17:59:59",
    "coefficient": 1.00
  },
  {
    "name": "evening",
    "start": "18:00:00",
    "end": "23:59:59",
    "coefficient": 1.5
  },
  {
    "name": "night",
    "start": "00:00:00",
    "end": "05:59:59",
    "coefficient": 2
  }
]'

exec dbo.usp_ParseJsonAddScreeningType @js
go