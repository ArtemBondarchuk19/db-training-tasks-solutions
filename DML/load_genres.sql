use CinemaDb
go

declare @json nvarchar(max) =
N'
    [
      {
        "name": "Action"
      },
      {
        "name": "Comedy"
      },
      {
        "name": "Drama"
      },
      {
        "name": "Fantasy"
      },
      {
        "name": "Horror"
      },
      {
        "name": "Romance"
      },
      {
        "name": "Thriller"
      },
      {
        "name": "Crime"
      },
      {
        "name": "Detective"
      }
    ]
'

exec dbo.usp_ParseJsonAddGenre
    @json = @json
go