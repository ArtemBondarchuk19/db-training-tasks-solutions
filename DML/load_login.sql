use CinemaDb;
go

declare @json nvarchar(max) = 
N'{
    "user": {
        "email": "test@test.com",
        "password": "af98a5e274df6ffd3a7787ed0b728a8b502a1b"
    }
}'
declare @result bit
exec dbo.usp_ParseJsonLogin @json, @result output
print @result
go

declare @json nvarchar(max) = 
N'{
    "user": {
        "email": "test1@test.com",
        "password": "af98a5e274df6ffd3a7787ed0b72"
    }
}'

declare @result bit
exec dbo.usp_ParseJsonLogin @json, @result output
print @result
go

declare @json nvarchar(max) = 
N'{
    "user": {
        "password": "af98a5e274df6ffd3a7787ed0b72"
    }
}'

declare @result bit
exec dbo.usp_ParseJsonLogin @json, @result output
print @result
go

declare @json nvarchar(max) = 
N'{
    "user": {
        "email": "test@test.com"
    }
}'

declare @result bit
exec dbo.usp_ParseJsonLogin @json, @result output
print @result
go