use CinemaDb
go

declare @cities nvarchar(max) =
N'
 [
  {
    "name": "Minsk",
    "country": {
      "name": "Belarus"
    }
  },
  {
    "name": "Brest",
    "country": {
      "name": "Belarus"
    }
  },
  {
    "name": "Bobruisk",
    "country": {
      "name": "Belarus"
    }
  },
  {
    "name": "Gomel",
    "country": {
      "name": "Belarus"
    }
  },
  {
    "name": "Grodno",
    "country": {
      "name": "Belarus"
    }
  },
  {
    "name": "Mogilev",
    "country": {
      "name": "Belarus"
    }
  },
  {
    "name": "Vitebsk",
    "country": {
      "name": "Belarus"
    }
  },
  {
    "name": "Lviv",
    "country": {
      "name": "Ukraine"
    }
  },
  {
    "name": "Kiev",
    "country": {
      "name": "Ukraine"
    }
  },
  {
    "name": "Moscow",
    "country": {
      "name": "Russia"
    }
  },
  {
    "name": "St-Petersburg",
    "country": {
      "name": "Russia"
    }
  },
  {
    "name": "Ufa",
    "country": {
      "name": "Russia"
    }
  },
  {
    "name": "Novosibirsk",
    "country": {
      "name": "Russia"
    }
  },
  {
    "name": "Warsaw",
    "country": {
      "name": "Poland"
    }
  },
  {
    "name": "Lodz",
    "country": {
      "name": "Poland"
    }
  },
  {
    "name": "Krakow",
    "country": {
      "name": "Poland"
    }
  },
  {
    "name": "Poznan",
    "country": {
      "name": "Poland"
    }
  },
  {
    "name": "London",
    "country": {
      "name": "England"
    }
  },
  {
    "name": "Birmingham",
    "country": {
      "name": "England"
    }
  },
  {
    "name": "Belfast",
    "country": {
      "name": "England"
    }
  },
  {
    "name": "Bladford",
    "country": {
      "name": "England"
    }
  },
  {
    "name": "Victoria",
    "country": {
      "name": "Canada"
    }
  },
  {
    "name": "Ottawa",
    "country": {
      "name": "Canada"
    }
  },
  {
    "name": "Toronto",
    "country": {
      "name": "Canada"
    }
  },
  {
    "name": "Whitehorse",
    "country": {
      "name": "Canada"
    }
  },
  {
    "name": "Paris",
    "country": {
      "name": "France"
    }
  },
  {
    "name": "Marseille",
    "country": {
      "name": "France"
    }
  },
  {
    "name": "Lyon",
    "country": {
      "name": "France"
    }
  },
  {
    "name": "Berlin",
    "country": {
      "name": "Germany"
    }
  },
  {
    "name": "Hamburg",
    "country": {
      "name": "Germany"
    }
  },
  {
    "name": "Munich",
    "country": {
      "name": "Germany"
    }
  },
  {
    "name": "Washington",
    "country": {
      "name": "USA"
    }
  },
  {
    "name": "New York",
    "country": {
      "name": "USA"
    }
  },
  {
    "name": "Los Angeles",
    "country": {
      "name": "USA"
    }
  }
 ]
'

exec dbo.usp_ParseJsonAddCity
    @json = @cities
go