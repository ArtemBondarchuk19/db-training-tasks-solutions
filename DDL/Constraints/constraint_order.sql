use CinemaDb;
go

exec dbo.usp_AddForeignKeyConstraint
	@childTableName		=	'dbo.[Order]',
	@childFieldsName	=	'UserId',
	@parentTableName	=	'dbo.[User]',
	@parentFieldsName	=	'UserId',
	@constraintName		=	'FK_Order_User'
go

exec dbo.usp_AddAlternateKeyConstraint
    @tableName      = 'dbo.[Order]',
    @fieldsName     = '[Date], UserId',
    @constraintName = 'AK_Order_Date_UserId'
go

exec dbo.usp_AddForeignKeyConstraint
	@childTableName		=	'dbo.[Order]',
	@childFieldsName	=	'PaymentId',
	@parentTableName	=	'dbo.Payment',
	@parentFieldsName	=	'PaymentId',
	@constraintName		=	'FK_Order_Payment'
go

exec dbo.usp_AddDefaultConstraint
	@tableName		=	'[Order]',
	@fieldName		=	'[Date]',
	@defaultValue	=	'getDate()',
	@constraintName =	'DF_Order_Date'
go