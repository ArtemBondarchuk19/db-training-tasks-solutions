use CinemaDb
go

exec dbo.usp_AddForeignKeyConstraint
	@childTableName		=	'dbo.MovieGenreBridge',
	@childFieldsName	=	'MovieId',
	@parentTableName	=	'dbo.Movie',
	@parentFieldsName	=	'MovieId',
	@constraintName		=	'FK_MovieGenreBridge_Movie'
go

exec dbo.usp_AddForeignKeyConstraint
	@childTableName		=	'dbo.MovieGenreBridge',
	@childFieldsName	=	'GenreId',
	@parentTableName	=	'dbo.Genre',
	@parentFieldsName	=	'GenreId',
	@constraintName		=	'FK_MovieGenreBridge_Genre'
go