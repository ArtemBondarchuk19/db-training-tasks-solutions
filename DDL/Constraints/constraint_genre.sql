use CinemaDb
go

exec dbo.usp_AddAlternateKeyConstraint
    @tableName      = 'dbo.Genre',
    @fieldsName     = 'Name',
    @constraintName = 'AK_Genre_Name'
go