use CinemaDb;
go

exec dbo.usp_AddForeignKeyConstraint
	@childTableName		=	'dbo.MovieCountryBridge',
	@childFieldsName	=	'MovieId',
	@parentTableName	=	'dbo.Movie',
	@parentFieldsName	=	'MovieId',
	@constraintName		=	'FK_MovieCountryBridge_Movie'
go

exec dbo.usp_AddForeignKeyConstraint
	@childTableName		=	'dbo.MovieCountryBridge',
	@childFieldsName	=	'CountryId',
	@parentTableName	=	'dbo.Country',
	@parentFieldsName	=	'CountryId',
	@constraintName		=	'FK_MovieCountryBridge_Country'
go
