﻿use CinemaDb
go

exec dbo.usp_AddAlternateKeyConstraint
    @tableName      = 'dbo.ScreeningType',
    @fieldsName     = 'Name',
    @constraintName = 'AK_ScreeningType_Name'
go

exec dbo.usp_AddCheckConstraint
	@tableName 		= 	'dbo.ScreeningType',
	@conditionExp 	= 	'[Start] < [End]',
	@constraintName = 	'CH_ScreeningType_Start_End'
go