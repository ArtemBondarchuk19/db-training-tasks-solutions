use CinemaDb;
go

exec dbo.usp_AddAlternateKeyConstraint
    @tableName      = 'dbo.Country',
    @fieldsName     = 'Name',
    @constraintName = 'AK_Country_Name'
go