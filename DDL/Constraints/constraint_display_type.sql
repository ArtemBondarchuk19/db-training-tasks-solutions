use CinemaDb
go

exec dbo.usp_AddAlternateKeyConstraint
    @tableName      = 'dbo.DisplayType',
    @fieldsName     = 'Name',
    @constraintName = 'AK_DisplayType_Name'
go