use CinemaDb;
go

exec dbo.usp_AddForeignKeyConstraint
	@childTableName		=	'dbo.City',
	@childFieldsName	=	'CountryId',
	@parentTableName	=	'dbo.Country',
	@parentFieldsName	=	'CountryId',
	@constraintName		=	'FK_City_Country'
go

exec dbo.usp_AddAlternateKeyConstraint
    @tableName      = 'dbo.City',
    @fieldsName     = 'Name, CountryId',
    @constraintName = 'AK_City_Name_CountryId'
go