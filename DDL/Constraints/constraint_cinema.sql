use CinemaDb;
go

exec dbo.usp_AddForeignKeyConstraint
	@childTableName		=	'dbo.Cinema',
	@childFieldsName	=	'CityId',
	@parentTableName	=	'dbo.City',
	@parentFieldsName	=	'CityId',
	@constraintName		=	'FK_Cinema_City'
go

exec dbo.usp_AddAlternateKeyConstraint
    @tableName      = 'dbo.Cinema',
    @fieldsName     = 'Name, Address, CityId',
    @constraintName = 'AK_Cinema_Name_Address_CityId'
go