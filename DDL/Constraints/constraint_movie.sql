﻿use CinemaDb
go

exec dbo.usp_AddAlternateKeyConstraint
    @tableName      = 'dbo.Movie',
    @fieldsName     = 'EIDR',
    @constraintName = 'AK_Movie_EIDR'
go


exec dbo.usp_AddCheckConstraint
    @tableName      = 'dbo.Movie',
    @conditionExp   = 'AgeLimit >= 0',
    @constraintName = 'CH_Movie_AgeLimit'
go

exec dbo.usp_AddCheckConstraint
    @tableName      = 'dbo.Movie',
    @conditionExp   = 'Duration > ''00:00:00''',
    @constraintName = 'CH_Movie_Duration'
go

exec dbo.usp_AddCheckConstraint
    @tableName      = 'dbo.Movie',
    @conditionExp   = 'Rating >=0 and Rating<=10',
    @constraintName = 'CH_Movie_Rating'
go