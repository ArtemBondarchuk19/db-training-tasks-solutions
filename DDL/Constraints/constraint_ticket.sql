﻿use CinemaDb;
go

exec dbo.usp_AddForeignKeyConstraint
	@childTableName		=	'dbo.Ticket',
	@childFieldsName	=	'ScreeningId',
	@parentTableName	=	'dbo.Screening',
	@parentFieldsName	=	'ScreeningId',
	@constraintName		=	'FK_Ticket_Screening'
go

exec dbo.usp_AddAlternateKeyConstraint
    @tableName      = 'dbo.Ticket',
    @fieldsName     = 'PlaceId, ScreeningId',
    @constraintName = 'AK_Ticket_PlaceId_ScreeningId'
go

exec dbo.usp_AddForeignKeyConstraint
	@childTableName		=	'dbo.Ticket',
	@childFieldsName	=	'PlaceId',
	@parentTableName	=	'dbo.Place',
	@parentFieldsName	=	'PlaceId',
	@constraintName		=	'FK_Ticket_Place'
go

exec dbo.usp_AddForeignKeyConstraint
	@childTableName		=	'dbo.Ticket',
	@childFieldsName	=	'OrderId',
	@parentTableName	=	'dbo.[Order]',
	@parentFieldsName	=	'OrderId',
	@constraintName		=	'FK_Ticket_Order'
go

exec dbo.usp_AddDefaultConstraint
	@tableName		=	'Ticket',
	@fieldName		=	'PrintDate',
	@defaultValue	=	'getDate()',
	@constraintName =	'DF_Ticket_PrintDate'
go

exec dbo.usp_AddCheckConstraint
	@tableName = 'dbo.Ticket',
	@conditionExp = 'FinalPrice >= 0',
	@constraintName = 'CH_Ticket_FinalPrice'
go