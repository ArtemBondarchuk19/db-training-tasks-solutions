use CinemaDb;
go

exec dbo.usp_AddDefaultConstraint
	@tableName		=	'Payment',
	@fieldName		=	'IsCompleted',
	@defaultValue	=	'0',
	@constraintName =	'DF_Payment_IsCompleted'
go

exec dbo.usp_AddDefaultConstraint
	@tableName		=	'Payment',
	@fieldName		=	'Date',
	@defaultValue	=	'getDate()',
	@constraintName =	'DF_Payment_Date'
go

exec dbo.usp_AddAlternateKeyConstraint
    @tableName      = 'dbo.Payment',
    @fieldsName     = 'PayGuid',
    @constraintName = 'AK_Payment_PayGuid'
go

exec dbo.usp_AddForeignKeyConstraint
	@childTableName		=	'dbo.Payment',
	@childFieldsName	=	'PaymentTypeId',
	@parentTableName	=	'dbo.PaymentType',
	@parentFieldsName	=	'PaymentTypeId',
	@constraintName		=	'FK_Payment_PaymentType'
go