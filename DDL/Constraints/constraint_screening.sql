﻿use CinemaDb
go

exec dbo.usp_AddForeignKeyConstraint
	@childTableName		=	'dbo.Screening',
	@childFieldsName	=	'MovieId',
	@parentTableName	=	'dbo.Movie',
	@parentFieldsName	=	'MovieId',
	@constraintName		=	'FK_Screening_Movie'
go

exec dbo.usp_AddAlternateKeyConstraint
    @tableName      = 'dbo.Screening',
    @fieldsName     = 'StartDate, EndDate, HallId',
    @constraintName = 'AK_Screening_StartDate_EndDate_HallId'
go

exec dbo.usp_AddForeignKeyConstraint
	@childTableName		=	'dbo.Screening',
	@childFieldsName	=	'ScreeningTypeId',
	@parentTableName	=	'dbo.ScreeningType',
	@parentFieldsName	=	'ScreeningTypeId',
	@constraintName		=	'FK_Screening_ScreeningType'
go

exec dbo.usp_AddForeignKeyConstraint
	@childTableName		=	'dbo.Screening',
	@childFieldsName	=	'DisplayTypeId',
	@parentTableName	=	'dbo.DisplayType',
	@parentFieldsName	=	'DisplayTypeId',
	@constraintName		=	'FK_Screening_DisplayType'
go

exec dbo.usp_AddForeignKeyConstraint
	@childTableName		=	'dbo.Screening',
	@childFieldsName	=	'HallId',
	@parentTableName	=	'dbo.Hall',
	@parentFieldsName	=	'HallId',
	@constraintName		=	'FK_Screening_Hall'
go

exec dbo.usp_AddCheckConstraint
	@tableName 			= 	'dbo.Screening',
	@conditionExp 		= 	'StartDate < EndDate',
	@constraintName 	= 	'CH_Screening_StartDate_EndDate'
go