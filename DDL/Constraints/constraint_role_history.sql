use CinemaDb;
go

exec dbo.usp_AddForeignKeyConstraint
    @childTableName     =   'dbo.RoleHistory',
    @childFieldsName    =   'UserId',
    @parentTableName    =   'dbo.[User]',
    @parentFieldsName   =   'UserId',
    @constraintName     =   'FK_RoleHistory_User'
go

exec dbo.usp_AddAlternateKeyConstraint
    @tableName      = 'dbo.RoleHistory',
    @fieldsName     = 'UserId, TargetUserId, DateModified',
    @constraintName = 'AK_RoleHistory_UserId_TargetUserId_DateModified'
go

exec dbo.usp_AddForeignKeyConstraint
    @childTableName     =   'dbo.RoleHistory',
    @childFieldsName    =   'TargetUserId',
    @parentTableName    =   'dbo.[User]',
    @parentFieldsName   =   'UserId',
    @constraintName     =   'FK_RoleHistory_TargetUser'
go

exec dbo.usp_AddForeignKeyConstraint
    @childTableName     =   'dbo.RoleHistory',
    @childFieldsName    =   'RoleId',
    @parentTableName    =   'dbo.[Role]',
    @parentFieldsName   =   'RoleId',
    @constraintName     =   'FK_RoleHistory_Role'
go

exec dbo.usp_AddDefaultConstraint
    @tableName      =   'RoleHistory',
    @fieldName      =   'DateModified',
    @defaultValue   =   'getDate()',
    @constraintName =   'DF_RoleHistory_DateModified'
go