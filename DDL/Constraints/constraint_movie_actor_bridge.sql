use CinemaDb
go

exec dbo.usp_AddForeignKeyConstraint
	@childTableName		=	'dbo.MovieActorBridge',
	@childFieldsName	=	'MovieId',
	@parentTableName	=	'dbo.Movie',
	@parentFieldsName	=	'MovieId',
	@constraintName		=	'FK_MovieActorBridge_Movie'
go

exec dbo.usp_AddForeignKeyConstraint
	@childTableName		=	'dbo.MovieActorBridge',
	@childFieldsName	=	'ActorId',
	@parentTableName	=	'dbo.Actor',
	@parentFieldsName	=	'ActorId',
	@constraintName		=	'FK_MovieActorBridge_Actor'
go