use CinemaDb;
go

exec dbo.usp_AddCheckConstraint
    @tableName = 'dbo.PlaceType',
    @conditionExp = 'Coefficient >= 0',
    @constraintName = 'CK_PlaceType_Coefficient'

exec dbo.usp_AddAlternateKeyConstraint
    @tableName      = 'dbo.PlaceType',
    @fieldsName     = 'Name',
    @constraintName = 'AK_PlaceType_Name'
go