use CinemaDb;
go

exec dbo.usp_AddForeignKeyConstraint
    @childTableName     =   'dbo.[User]',
    @childFieldsName    =   'RoleId',
    @parentTableName    =   'dbo.[Role]',
    @parentFieldsName   =   'RoleId',
    @constraintName     =   'FK_User_Role'
go

exec dbo.usp_AddAlternateKeyConstraint
    @tableName      = 'dbo.[User]',
    @fieldsName     = 'Email',
    @constraintName = 'AK_User_Email'
go

exec dbo.usp_AddAlternateKeyConstraint
    @tableName      = 'dbo.[User]',
    @fieldsName     = 'Phone',
    @constraintName = 'AK_User_Phone'
go

exec dbo.usp_AddDefaultConstraint 
    @tableName      =   'dbo.[User]', 
    @fieldName      =   'IsPhoneConfirmed',
    @defaultValue   =   '0',
    @constraintName =   'DF_User_IsPhoneConfirmed'
go

exec dbo.usp_AddDefaultConstraint 
    @tableName      =   'dbo.[User]', 
    @fieldName      =   'IsEmailConfirmed',
    @defaultValue   =   '0',
    @constraintName =   'DF_User_IsEmailConfirmed'
go

exec dbo.usp_AddDefaultConstraint 
    @tableName      =   'dbo.[User]', 
    @fieldName      =   'RegistrationDate',
    @defaultValue   =   'getDate()',
    @constraintName =   'DF_User_IsRegistrationDateConfirmed'
go

exec dbo.usp_AddDefaultConstraint
    @tableName      = 'dbo.[User]',
    @fieldName      = 'RoleId',
    @defaultValue   = 'dbo.ufn_GetDefaultUserRoleId()',
    @constraintName = 'DF_User_RoleId'
go

exec dbo.usp_AddCheckConstraint
    @tableName      = 'dbo.[User]',
    @conditionExp   = 'dbo.ufn_IsValidEmail(Email) = 1',
    @constraintName = 'CH_User_Email'
go