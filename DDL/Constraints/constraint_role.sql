use CinemaDb;
go

exec dbo.usp_AddAlternateKeyConstraint
    @tableName      = 'dbo.[Role]',
    @fieldsName     = 'Name',
    @constraintName = 'AK_Role_Name'
go