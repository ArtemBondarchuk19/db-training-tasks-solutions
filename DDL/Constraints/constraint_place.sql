﻿use CinemaDb;
go

exec dbo.usp_AddForeignKeyConstraint
	@childTableName		=	'dbo.Place',
	@childFieldsName	=	'PlaceTypeId',
	@parentTableName	=	'dbo.PlaceType',
	@parentFieldsName	=	'PlaceTypeId',
	@constraintName		=	'FK_Place_PlaceType'
go

exec dbo.usp_AddForeignKeyConstraint
	@childTableName		=	'dbo.Place',
	@childFieldsName	=	'HallId',
	@parentTableName	=	'dbo.Hall',
	@parentFieldsName	=	'HallId',
	@constraintName		=	'FK_Place_Hall'
go

exec dbo.usp_AddAlternateKeyConstraint
    @tableName      = 'dbo.Place',
    @fieldsName     = 'RowNumber, SeatNumber, HallId',
    @constraintName = 'AK_Place_RowNumber_SeatNumber_HallId'
go

exec dbo.usp_AddCheckConstraint
	@tableName		= 'dbo.Place',
	@conditionExp	= 'RowNumber > 0',
	@constraintName = 'CH_Place_RowNumber'
go

exec dbo.usp_AddCheckConstraint
	@tableName		= 'dbo.Place',
	@conditionExp	= 'SeatNumber > 0',
	@constraintName = 'CH_Place_SeatNumber'
go