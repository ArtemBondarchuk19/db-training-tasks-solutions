use CinemaDb
go

exec dbo.usp_AddAlternateKeyConstraint
    @tableName      = 'dbo.Actor',
    @fieldsName     = 'FirstName, LastName, CountryId',
    @constraintName = 'AK_Actor_FirstName_LastName_CountryId'
go

exec dbo.usp_AddForeignKeyConstraint
	@childTableName		=	'dbo.Actor',
	@childFieldsName	=	'CountryId',
	@parentTableName	=	'dbo.Country',
	@parentFieldsName	=	'CountryId',
	@constraintName		=	'FK_Actor_Country'
go