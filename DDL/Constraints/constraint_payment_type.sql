use CinemaDb
go

exec dbo.usp_AddAlternateKeyConstraint
    @tableName      = 'dbo.PaymentType',
    @fieldsName     = 'Name',
    @constraintName = 'AK_PaymentType_Name'
go