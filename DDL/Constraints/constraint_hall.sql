﻿use CinemaDb;
go

exec dbo.usp_AddForeignKeyConstraint
	@childTableName		=	'dbo.Hall',
	@childFieldsName	=	'CinemaId',
	@parentTableName	=	'dbo.Cinema',
	@parentFieldsName	=	'CinemaId',
	@constraintName		=	'FK_Hall_Cinema'
go

exec dbo.usp_AddAlternateKeyConstraint
    @tableName      = 'dbo.Hall',
    @fieldsName     = 'Name, CinemaId',
    @constraintName = 'AK_Hall_Name_CinemaId'
go

exec dbo.usp_AddCheckConstraint
	@tableName		= 'dbo.Hall',
	@conditionExp	= '[Floor] > 0',
	@constraintName = 'CH_Hall_Floor'
go