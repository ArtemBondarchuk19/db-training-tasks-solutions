use CinemaDb
go

if object_id('dbo.City','U') is not null
    drop table dbo.City
go

create table dbo.City
(
    CityId          uniqueidentifier    not null    default     NEWSEQUENTIALID(),
    [Name]          nvarchar(50)        not null,
    CountryId       uniqueidentifier    not null,

    constraint PK_City_CityId primary key (CityId)
)
go