use CinemaDb
go

if object_id('dbo.MovieCountryBridge','U') is not null
    drop table dbo.MovieCountryBridge
go

create table dbo.MovieCountryBridge
(
    MovieId         uniqueidentifier        not null,
    CountryId       uniqueidentifier        not null,

    constraint PK_MovieCountryBridge_MovieId_CountryId primary key (
        MovieId,
        CountryId
    )
)
go