use CinemaDb
go

if object_id('dbo.DisplayType', 'U') is not null
    drop table dbo.DisplayType
go

create table dbo.DisplayType
(
    DisplayTypeId       tinyint             not null    identity,
    [Name]              nvarchar(5)         not null

    constraint PK_DisplayType_DisplayTypeId primary key(
        DisplayTypeId
    )
)
go