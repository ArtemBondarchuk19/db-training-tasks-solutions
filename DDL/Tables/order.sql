use CinemaDb;
go

if object_id('dbo.Order','U') is not null
	drop table dbo.[Order];
go

create table dbo.[Order]
(
    OrderId         uniqueidentifier    not null    default     NEWSEQUENTIALID(),
    [Date]          datetime2           not null,
    UserId          uniqueidentifier    not null,
    PaymentId       uniqueidentifier

    constraint PK_Order_OrderId primary key(OrderId)
)
go