use CinemaDb
go

if object_id('dbo.Actor', 'U') is not null
	drop table dbo.Actor
go

create table dbo.Actor
(
    ActorId         uniqueidentifier    not null    default     NEWSEQUENTIALID(),
    [FirstName]     nvarchar(50)        not null,
    [LastName]      nvarchar(50)        not null,
    CountryId       uniqueidentifier

	constraint PK_Actor_ActorId primary key(
		ActorId
	)
)
go
