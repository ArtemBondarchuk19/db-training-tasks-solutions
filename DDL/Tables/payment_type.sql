use CinemaDb
go

if object_id('dbo.PaymentType','U') is not null
    drop table dbo.PaymentType
go

create table dbo.PaymentType
(
    PaymentTypeId       tinyint             not null    identity,
    [Name]              nvarchar(50)        not null

    constraint PK_PaymentType_PaymentTypeId primary key(PaymentTypeId)
)
go