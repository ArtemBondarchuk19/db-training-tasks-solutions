use CinemaDb
go

if object_id('dbo.Movie', 'U') is not null
    drop table dbo.Movie
go

create table dbo.Movie
(
    MovieId             uniqueidentifier    not null    default     NEWSEQUENTIALID(),
    EIDR                nvarchar(34)        not null,
    [Name]              nvarchar(50)        not null,
    ReleaseDate         date                not null,
    Duration            time                not null,
    AgeLimit            tinyint             not null,
    Rating              decimal(3,1)

    constraint PK_Movie_MovieId primary key(
        MovieId
    )
)
go