use CinemaDb
go

if object_id('dbo.ScreeningType', 'U') is not null
	drop table dbo.ScreeningType
go

create table dbo.ScreeningType
(
    ScreeningTypeId         tinyint                 not null    identity,
    [Name]                  nvarchar(10)            not null,
    [Start]                 time                    not null,
    [End]                   time                    not null,
    Coefficient             decimal(3, 2)           not null

    constraint PK_ScreeningType_ScreeningTypeId primary key(
        ScreeningTypeId
    )
)
go