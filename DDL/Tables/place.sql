use CinemaDb
go

if object_id('dbo.Place','U') is not null
    drop table dbo.Place
go

create table dbo.Place
(
    PlaceId         uniqueidentifier    not null    default     NEWSEQUENTIALID(),
    RowNumber       tinyint             not null,
    SeatNumber      tinyint             not null,
    PlaceTypeId     tinyint             not null,
    HallId          uniqueidentifier    not null, 

    constraint PK_Place_PlaceId primary key (PlaceId)
)
go