use CinemaDb
go

if object_id('dbo.Genre', 'U') is not null
	drop table dbo.Genre
go

create table dbo.Genre
(
    GenreId         uniqueidentifier    not null    default     NEWSEQUENTIALID(),
    [Name]          nvarchar(20)        not null

	constraint PK_Genre_GenreId primary key(
		GenreId
	)
)
go