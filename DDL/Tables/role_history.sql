use CinemaDb;
go

if object_id('dbo.RoleHistory','U') is not null
	drop table dbo.RoleHistory;
go

create table dbo.RoleHistory
(
    RoleHistoryId       uniqueidentifier        not null    default     NEWSEQUENTIALID(),
    DateModified        datetime2(2)            not null,
    UserId              uniqueidentifier        not null,
    TargetUserId        uniqueidentifier        not null,
    RoleId              tinyint                 not null

    constraint PK_RoleHistory_RoleHistoryId primary key(RoleHistoryId)
)
go