use CinemaDb
go

if object_id('dbo.Country','U') is not null
    drop table dbo.Country
go

create table dbo.Country
(
    CountryId       uniqueidentifier    not null    default     NEWSEQUENTIALID(),
    [Name]          nvarchar(50)        not null,

    constraint PK_Country_CountryId primary key (CountryId)
)
go