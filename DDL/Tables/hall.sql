use CinemaDb
go

if object_id('dbo.Hall','U') is not null
    drop table dbo.Hall
go

create table dbo.Hall
(
    HallId                  uniqueidentifier    not null    default     NEWSEQUENTIALID(),
    [Name]                  nvarchar(50)        not null,
    [Floor]                 tinyint             not null,
    DefaultTicketPrice      money               not null,
    CinemaId                uniqueidentifier    not null,

    constraint PK_Hall_HallId primary key (HallId)
)
go