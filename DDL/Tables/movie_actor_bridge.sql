use CinemaDb
go

if object_id('dbo.MovieActorBridge', 'U') is not null
	drop table dbo.MovieActorBridge
go

create table dbo.MovieActorBridge
(
	MovieId			uniqueidentifier			not null,
	ActorId			uniqueidentifier			not null

	constraint PK_MovieActorBridge_MovieId_ActorId primary key(
		MovieId,
		ActorId
	)
)
go