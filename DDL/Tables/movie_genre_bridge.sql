use CinemaDb
go

if object_id('dbo.MovieGenreBridge', 'U') is not null
	drop table dbo.MovieGenreBridge
go

create table dbo.MovieGenreBridge
(
    MovieId         uniqueidentifier            not null,
    GenreId         uniqueidentifier            not null

	constraint PK_MovieGenreBridge_MovieId_GenreId primary key(
		MovieId,
		GenreId
	)
)
go