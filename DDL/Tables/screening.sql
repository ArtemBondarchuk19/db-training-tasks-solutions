use CinemaDb
go

if object_id('dbo.Screening', 'U') is not null
	drop table dbo.Screening
go

create table dbo.Screening
(
    ScreeningId             uniqueidentifier        not null    default     NEWSEQUENTIALID(),
    StartDate               smalldatetime           not null,
    EndDate                 smalldatetime           not null,
    MovieId                 uniqueidentifier        not null,
    ScreeningTypeId         tinyint                 not null,
    DisplayTypeId           tinyint                 not null,
    HallId                  uniqueidentifier        not null,

    constraint PK_Screening_ScreeningId primary key(
        ScreeningId
    )
)
go