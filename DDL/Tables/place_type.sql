use CinemaDb
go

if object_id('dbo.PlaceType','U') is not null
    drop table dbo.PlaceType
go

create table dbo.PlaceType
(
    PlaceTypeId         tinyint             not null    identity,
    [Name]              nvarchar(50)        not null,
    Coefficient         decimal(3, 2)       not null

    constraint PK_PlaceType_PlaceTypeId primary key (PlaceTypeId)
)
go