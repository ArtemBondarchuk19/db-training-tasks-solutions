use CinemaDb;
go

if object_id('dbo.User','U') is not null
	drop table dbo.[User];
go

create table dbo.[User]
(
    UserId                  uniqueidentifier        not null    default     NEWSEQUENTIALID(),
    Email                   nvarchar(100)           not null,
    Phone                   nvarchar(25)            not null,
    PasswordHash            varbinary(20)           not null,
	Salt		            char(25)				not null,
    RegistrationDate        datetime2(2)            not null,
    IsPhoneConfirmed        bit                     not null,
    IsEmailConfirmed        bit                     not null,
    RoleId                  tinyint                 not null

    constraint PK_User_UserId primary key(UserId)
)
go