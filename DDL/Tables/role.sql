use CinemaDb;
go

if object_id('dbo.Role','U') is not null
	drop table dbo.[Role];
go

create table dbo.[Role]
(
    RoleId      tinyint             not null        identity,
    [Name]      nvarchar(50)        not null

    constraint PK_Role_RoleId primary key(RoleId)
)
go