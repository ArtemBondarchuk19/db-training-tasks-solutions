use CinemaDb
go

if object_id('dbo.Cinema','U') is not null
    drop table dbo.Cinema
go

create table dbo.Cinema
(
    CinemaId        uniqueidentifier    not null    default     NEWSEQUENTIALID(),
    [Name]          nvarchar(50)        not null,
    [Address]       nvarchar(200)       not null,
    CityId          uniqueidentifier    not null,

    constraint PK_Cinema_CinemaId primary key (CinemaId)
)
go