use CinemaDb
go

if object_id('dbo.CinemaJson','U') is not null
    drop table dbo.CinemaJson
go

create table dbo.CinemaJson
(
    [Data]       nvarchar(max)       not null
)
go