use CinemaDb
go

if object_id('dbo.Make_order_json','U') is not null
    drop table dbo.Make_order_json
go

create table dbo.Make_order_json
(
    [Data]       nvarchar(max)       not null
)
go