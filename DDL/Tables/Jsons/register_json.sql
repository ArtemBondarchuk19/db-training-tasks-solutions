use CinemaDb
go

if object_id('dbo.RegisterJson','U') is not null
    drop table dbo.RegisterJson
go

create table dbo.RegisterJson
(
    [Data]       nvarchar(max)       not null
)
go