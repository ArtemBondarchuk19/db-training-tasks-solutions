use CinemaDb
go

if object_id('dbo.HallJson','U') is not null
    drop table dbo.HallJson
go

create table dbo.HallJson
(
    [Data]       nvarchar(max)       not null
)
go