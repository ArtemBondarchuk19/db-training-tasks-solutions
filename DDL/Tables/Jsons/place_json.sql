use CinemaDb
go

if object_id('dbo.PlaceJson','U') is not null
    drop table dbo.PlaceJson
go

create table dbo.PlaceJson
(
    [Data]       nvarchar(max)       not null
)
go