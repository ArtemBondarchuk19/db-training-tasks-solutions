use CinemaDb
go

if object_id('dbo.MovieJson', 'U') is not null
    drop table dbo.MovieJson
go

create table dbo.MovieJson
(
    [Data]      nvarchar(max)       not null
)
go