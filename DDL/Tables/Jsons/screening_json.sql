use CinemaDb
go

if object_id('dbo.ScreeningJson', 'U') is not null
    drop table dbo.ScreeningJson
go

create table dbo.ScreeningJson
(
    [Data]      nvarchar(max)       not null
)
go