use CinemaDb;
go

if object_id('dbo.Ticket','U') is not null
	drop table dbo.[Ticket];
go

create table dbo.[Ticket]
(
    TicketId        uniqueidentifier        not null    default     NEWSEQUENTIALID(),
    PrintDate       datetime2               not null,
    FinalPrice      money                   not null,
    ScreeningId     uniqueidentifier        not null,
    PlaceId         uniqueidentifier        not null,
    OrderId         uniqueidentifier

    constraint PK_Ticket_TicketId primary key(TicketId)
)
go