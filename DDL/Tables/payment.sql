use CinemaDb;
go

if object_id('dbo.Payment','U') is not null
	drop table dbo.Payment;
go

create table dbo.Payment
(
    PaymentId           uniqueidentifier    not null    default     NEWSEQUENTIALID(),
    PayGuid             uniqueidentifier    not null,
    PaymentTypeId       tinyint             not null,
    [Date]              datetime2           not null,
    IsCompleted         bit                 not null

    constraint PK_Payment_PaymentId primary key(PaymentId)
)
go