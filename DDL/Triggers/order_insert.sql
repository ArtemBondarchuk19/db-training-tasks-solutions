use CinemaDb
go

if object_id('dbo.order_insert') is not null
    drop trigger dbo.order_insert
go

create trigger      dbo.order_insert
on                  dbo.[Order]
after               insert
as
declare @paymentGuid uniqueidentifier
set @paymentGuid = NEWID()

declare @random tinyint
set @random = FLOOR(RAND()*100)+1

declare @isCompleted bit
set @isCompleted = case
    when @random <= 10 then 0
    else 1
end

create table        #paymentTypes
(
    RandomId        uniqueidentifier,
    PaymentTypeId   tinyint
)

insert into         #paymentTypes
                    (
                        RandomId,
                        PaymentTypeId
                    )
select              newid(),
                    pt.PaymentTypeId
from                dbo.PaymentType pt

insert into         dbo.Payment
values
(
    @paymentGuid,
    NEWID(),
    (
        select top 1        pt.PaymentTypeId
        from                #paymentTypes pt
        where               pt.RandomId
        in
        (
            select          min(RandomId)
            from            #paymentTypes
        )
    ),
    SYSDATETIME(),
    @isCompleted
)

update              dbo.[Order]
set                 PaymentId = @paymentGuid
from                [Order] o
join                inserted on o.OrderId = inserted.OrderId
go