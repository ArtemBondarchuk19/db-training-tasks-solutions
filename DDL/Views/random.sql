use CinemaDb
go

if object_id('dbo.Random', 'V') is not null
    drop view dbo.Random
go

create view dbo.Random
as
select rand() RandomValue
go