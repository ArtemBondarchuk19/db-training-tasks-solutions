use CinemaDb
go

if object_id('dbo.udf_MostSaledMoviePerMonth') is not null
    drop function dbo.udf_MostSaledMoviePerMonth
go

CREATE FUNCTION dbo.udf_MostSaledMoviePerMonth
(
    @CountryName nvarchar(50),
    @Month date
)
RETURNS TABLE
AS
RETURN

        select top 1        m.[Name] as Movie, count(*) as TicketCount
        from                (select * from dbo.Country 
        where               dbo.Country.[Name] = @CountryName) c
        join                dbo.MovieCountryBridge bridge on c.CountryId = bridge.CountryId
        join                dbo.Movie m on bridge.MovieId = m.MovieId
        join                dbo.Screening sc on m.MovieId = sc.MovieId
        join                dbo.Ticket t on t.ScreeningId = sc.ScreeningId
        join                dbo.[Order] o on t.OrderId = o.OrderId
        join                dbo.Payment p on o.PaymentId = p.PaymentId
        where               year(sc.StartDate) = year(@Month)
                            and month(sc.StartDate) = month(@Month)
                            and p.IsCompleted = 1
        group by            m.[Name],
                            m.[EIDR]
        order by            TicketCount desc