use CinemaDb
go

if object_id('dbo.ufn_GetScreeningTypeId', 'FN') is not null
    drop function dbo.ufn_GetScreeningTypeId
go

create function dbo.ufn_GetScreeningTypeId
(
    @startDate  datetime
)
returns tinyint
as
begin
    declare @id tinyint =
        (
            select st.ScreeningTypeId
            from dbo.ScreeningType st
            where cast(@startDate as time) between st.Start and st.[End]
        )
return @id
end
go