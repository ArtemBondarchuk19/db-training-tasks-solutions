use CinemaDb
go

if object_id('dbo.ufn_GetRandomValue', 'FN') is not null
    drop function dbo.ufn_GetRandomValue
go

create function dbo.ufn_GetRandomValue()
returns float
as
begin
    declare @randomValue float = (select RandomValue from dbo.Random)
    return @randomValue
end
go