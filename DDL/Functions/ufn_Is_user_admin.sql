use CinemaDb;
go

if object_id('dbo.ufn_IsUserAdmin', 'FN') is not null
    drop function dbo.ufn_IsUserAdmin
go

create function dbo.ufn_IsUserAdmin
(
	@json nvarchar(max)
)
returns bit
as
begin
if exists 
	(
		select			1
		from			openjson(@json, '$.user')
						with
						(
							Email varchar(50) '$.email'
						) json_user
		join			dbo.[User] on json_user.Email = dbo.[User].Email
		join			dbo.[Role] on dbo.[User].RoleId = dbo.[Role].RoleId
		where			dbo.[Role].[Name] = 'admin'
	)
	begin
	return 1
	end
return 0
end
go