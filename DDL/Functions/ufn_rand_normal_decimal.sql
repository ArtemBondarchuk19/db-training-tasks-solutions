use CinemaDb;
go

if object_id('dbo.ufn_RandNormalDecimal', 'FN') is not null
    drop function dbo.ufn_RandNormalDecimal
go

create function dbo.ufn_RandNormalDecimal
(
    @Mean float,
    @StDev float,
    @Rand float
)
returns float
as
begin
    declare @res float
    set @res = (@StDev * SQRT(-2 * LOG(@Rand))*COS(2*ACOS(-1.)*@Rand)) + @Mean
    if (@res<=0)
        return @Mean
    return Round(@res,2)
end
go