use CinemaDb;
go

if object_id('dbo.ufn_GetDefaultUserRoleId', 'FN') is not null
    drop function dbo.ufn_GetDefaultUserRoleId
go

create function dbo.ufn_GetDefaultUserRoleId()
returns tinyint
as
begin
    declare @userRoleId tinyint

    select @userRoleId = ur.RoleId
    from   dbo.[Role] ur
    where  ur.[Name] = 'user'

    return @userRoleId
end
go