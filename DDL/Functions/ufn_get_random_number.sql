use CinemaDb
go

if object_id('dbo.ufn_GetRandomNumber', 'FN') is not null
    drop function dbo.ufn_GetRandomNumber
go

create function dbo.ufn_GetRandomNumber
(
    @min int,
    @max int
)
returns int
as
begin
    declare @randomValue float = (select RandomValue from dbo.Random)
    declare @number int = @min + floor(@randomValue * ((@max - @min) + 1))
return @number
end
go