use CinemaDb;
go

if object_id('dbo.ufn_RandNormal', 'FN') is not null
    drop function dbo.ufn_RandNormal
go

create function dbo.ufn_RandNormal
(
    @Mean int,
    @StDev int,
    @Rand float
)
returns int
as
begin
    declare @res int
    set @res = cast(round((@StDev * SQRT(-2 * LOG(@Rand))*COS(2*ACOS(-1.)*@Rand)) + @Mean, 0) as int)
    if (@res<=0)
        return @Mean
    return @res
end
go