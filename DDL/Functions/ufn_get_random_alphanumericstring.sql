use CinemaDb
go

if object_id('dbo.ufn_GetRandomAlphaNumericString', 'FN') is not null
    drop function dbo.ufn_GetRandomAlphaNumericString
go

create function dbo.ufn_GetRandomAlphaNumericString
(
    @minLength int,
    @maxLength int
)
returns nvarchar(max)
as
begin
    declare @string nvarchar(max) = ''
    declare @stringLength int = dbo.ufn_GetRandomNumber(@minLength, @maxLength)
    declare @counter int = 1

    while @counter <= @stringLength
    begin
        if dbo.ufn_GetRandomValue() > 0.5
        begin
            set @string = concat(@string, char(dbo.ufn_GetRandomNumber(65, 90)))
        end
        else
        begin
            set @string = concat(@string, dbo.ufn_GetRandomNumber(0, 9))
        end

        set @counter = @counter + 1
    end
return @string
end
go