use CinemaDb;
go

if object_id('dbo.ufn_GetCapitals', 'FN') is not null
    drop function dbo.ufn_GetCapitals
go

create function dbo.ufn_GetCapitals()
returns table
as
return
(
    select              ci.CityId
    from                dbo.City ci
    join                dbo.Country co on ci.CountryId = co.CountryId
    where               ci.[Name] = 'Minsk' and co.[Name] = 'Belarus' or
                        ci.[Name] = 'Kiev' and co.[Name] = 'Ukraine' or
                        ci.[Name] = 'Moscow' and co.[Name] = 'Russia' or
                        ci.[Name] = 'Warsaw' and co.[Name] = 'Poland' or
                        ci.[Name] = 'London' and co.[Name] = 'England' or
                        ci.[Name] = 'Ottawa' and co.[Name] = 'Canada' or
                        ci.[Name] = 'Paris' and co.[Name] = 'France' or
                        ci.[Name] = 'Berlin' and co.[Name] = 'Germany' or
                        ci.[Name] = 'Washington' and co.[Name] = 'USA'
)
go