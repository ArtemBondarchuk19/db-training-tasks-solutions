use CinemaDb;
go

if object_id('dbo.ufn_IsValidEmail', 'FN') is not null
    drop function dbo.ufn_IsValidEmail
go

create function dbo.ufn_IsValidEmail
(
    @email  nvarchar(200)
)
returns tinyint
as
begin
    declare @id bit = 
    case 
        when @email LIKE '%_@__%.__%' AND PATINDEX('%[^a-z,0-9,@,.,_]%',@email) = 0
            then 1
        else 0
    end
return @id
end
go