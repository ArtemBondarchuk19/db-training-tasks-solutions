use CinemaDb
go

if object_id('dbo.usp_ParseJsonAddCity', 'P') is not null
    drop procedure dbo.usp_ParseJsonAddCity
go

create proc dbo.usp_ParseJsonAddCity
(
    @json nvarchar(max)
)
as
begin
    insert into     dbo.City 
                    (
                        [Name], 
                        CountryId
                    )
    select          js_cities.CityName, 
                    dbo.Country.CountryId
    from            openjson(@json)
                    with
                    (
                        CityName        varchar(50)     '$.name' ,
                        CountryName     varchar(50)     '$.country.name' 
                    ) js_cities
    join            dbo.Country on js_cities.CountryName = dbo.Country.[Name]
    left join       dbo.City on js_cities.CityName = dbo.City.[Name] 
                    and dbo.Country.CountryId = dbo.City.CountryId
    where           dbo.City.[Name] is null
end
go