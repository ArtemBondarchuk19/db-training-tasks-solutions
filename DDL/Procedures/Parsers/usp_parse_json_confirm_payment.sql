use CinemaDb;
go

if object_id('dbo.usp_ParseJsonConfirmPayment','P') is not null
    drop procedure dbo.usp_ParseJsonConfirmPayment
go

create procedure dbo.usp_ParseJsonConfirmPayment
(
    @json nvarchar(max)
)
as
begin
    begin try
        begin transaction
            create table #JsonConfirmPayment
            (
                PaymentTypeId   tinyint,
                IsCompleted     bit,
                PayGuid         uniqueidentifier,
                [Date]          datetime,
                Email           nvarchar(100)
            )
            insert into #JsonConfirmPayment 
            (
                PaymentTypeId,
                IsCompleted,
                PayGuid,
                [Date],
                Email
            )
            select          pt.PaymentTypeId,
                            jcp.IsCompleted,
                            jcp.PayGuid,
                            jcp.[Date],
                            jcp.Email
            from            openjson(@json)
                            with
                            (
                                PaymentTypeName     nvarchar(50)        '$.paymentType.name',
                                IsCompleted         bit                 '$.payment.isCompleted',
                                PayGuid             uniqueidentifier    '$.payment.payGuid',
                                [Date]              datetime            '$.order.date',
                                Email               nvarchar(100)       '$.order.user.email'
                            ) jcp
            join            dbo.PaymentType pt  on      pt.[Name] = jcp.PaymentTypeName
            join            dbo.[Order] o       on      o.[Date] = jcp.[Date]
            join            dbo.[User] u        on      (
                                                            u.Email = jcp.Email and 
                                                            u.UserId = o.UserId
                                                        )

            insert into dbo.Payment 
            (
                PayGuid,
                PaymentTypeId,
                [Date],
                IsCompleted
            )
            select          jcp.PayGuid,
                            jcp.PaymentTypeId,
                            getDate(),
                            jcp.IsCompleted
            from            #JsonConfirmPayment jcp
            left join       dbo.Payment p on jcp.PayGuid = p.PayGuid
            where           p.PayGuid is null

            update          o
            set             PaymentId = p.PaymentId
            from            #JsonConfirmPayment jcr 
            join            dbo.Payment p       on      p.PayGuid = jcr.PayGuid
            join            dbo.[Order] o       on      o.[Date] = jcr.[Date]
            join            dbo.[User] u        on      u.UserId = o.UserId
            where           jcr.Email = u.Email
        commit
    end try
    begin catch
        print 'Oops. There were issues when executing [dbo].[usp_ParseJsonConfirmPayment]'
        rollback
    end catch
 end
go