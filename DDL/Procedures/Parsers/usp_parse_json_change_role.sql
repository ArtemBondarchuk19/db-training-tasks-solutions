use CinemaDb;
go

if object_id('dbo.usp_ParseJsonChangeRole','P') is not null
    drop procedure dbo.usp_ParseJsonChangeRole
go

create procedure dbo.usp_ParseJsonChangeRole
(
    @json nvarchar(max)
)
as
begin
    begin try 
        begin transaction
            create table #JsonRoleHistory
            (
                UserId          uniqueidentifier,
                TargetUserId    uniqueidentifier,
                RoleId          tinyint,
                DateModified    datetime
            )

            insert into #JsonRoleHistory
            (
                UserId,
                TargetUserId,
                RoleId,
                DateModified
            )
            select      a.UserId,
                        u.UserId,
                        r.RoleId,
                        getdate()
            from openjson(@json)
            with 
            (
                adminEmail      nvarchar(100)   '$.user.email',
                users           nvarchar(max)   '$.users'           as json,
                roleName        nvarchar(50)    '$.role.name'
            ) jcr
            outer apply openjson(jcr.users)
            with
            (
                targetEmail     nvarchar(100)   '$.email'
            ) jcru
            join dbo.[User] a	on	jcr.adminEmail = a.Email
            join dbo.[Role] r	on	jcr.roleName = r.[Name]
            join dbo.[User] u	on	jcru.targetEmail = u.Email
            
            insert into dbo.RoleHistory
            (
                UserId,
                TargetUserId,
                RoleId
            )
            select          jrh.UserId,
                            jrh.TargetUserId,
                            jrh.RoleId
            from            #JsonRoleHistory jrh
            left join       dbo.RoleHistory rh      on      (
                                                                jrh.RoleId          =       rh.RoleId and
                                                                jrh.UserId          =       rh.UserId and
                                                                jrh.TargetUserId    =       rh.TargetUserId and
                                                                jrh.DateModified    =       rh.DateModified
                                                            )
            where           rh.DateModified is null

            update          u
            set             RoleId = jrh.RoleId 
            from            dbo.[User] u
            join            #JsonRoleHistory jrh 
            on              u.UserId = jrh.TargetUserId
       commit
    end try
    begin catch
        print 'Oops. There were issues when executing [dbo].[usp_ParseJsonChangeRole]'
        rollback
    end catch
end
go