use CinemaDb;
go

if object_id('dbo.usp_ParseJsonMakeOrder','P') is not null
    drop procedure dbo.usp_ParseJsonMakeOrder
go

create procedure dbo.usp_ParseJsonMakeOrder
(
    @json nvarchar(max)
)
as
begin
    begin try
        begin transaction
            create table #JsonMakeOrder
            (
                CinemaId        uniqueidentifier,
                CityId          uniqueidentifier,
                CountryId       uniqueidentifier,
                HallId          uniqueidentifier,
                PlaceId         uniqueidentifier,
                ScreeningId     uniqueidentifier,
                UserId          uniqueidentifier,
                OrderId         uniqueidentifier,
                CurrentDate     datetime
            )

            insert into         #JsonMakeOrder
            select              c.CinemaId,
                                ct.CityId,
                                ctr.CountryId,
                                h.HallId,
                                p.PlaceId,
                                sc.ScreeningId,
                                u.UserId,
                                t.OrderId,
                                getDate()
            from                openjson(@json)
                                with
                                (
                                    [Address]       nvarchar(200)       '$.cinema.address',
                                    CinemaName      nvarchar(50)        '$.cinema.name',
                                    CityName        nvarchar(50)        '$.cinema.city.name',
                                    CountryName     nvarchar(50)        '$.cinema.city.country.name',
                                    HallName        nvarchar(50)        '$.screening.hall.name',
                                    Places          nvarchar(max)       '$.place'                           as json,
                                    StartDate       smalldatetime       '$.screening.startDate',
                                    EndDate         smalldatetime       '$.screening.endDate',
                                    Email           nvarchar(100)       '$.user.email'
                                ) jmo
        outer apply             openjson(jmo.Places)
                                with
                                (
                                    RowNumber       tinyint     '$.rowNumber',
                                    SeatNumber      tinyint     '$.seatNumber'
                                ) jmop
        join                    dbo.Country ctr on      jmo.CountryName = ctr.[Name]
        join                    dbo.City ct     on      jmo.CityName   = ct.[Name] and 
                                                        ctr.CountryId  = ct.CountryId
        join                    dbo.Cinema c    on      jmo.CinemaName          = c.[Name] and
                                                        jmo.[Address] 	        = c.[Address] and
                                                        ct.CityId		= c.CityId

        join                    dbo.Hall h       on     c.CinemaId       = h.CinemaId and
                                                        jmo.HallName = h.[Name]
        join                    dbo.Screening sc on     sc.StartDate = jmo.StartDate and
                                                        sc.EndDate   = jmo.EndDate   and
                                                        sc.HallId    = h.HallId
        join                    dbo.[User] u     on     u.Email  = jmo.Email
        join                    dbo.Place p      on     p.SeatNumber = jmop.SeatNumber and
                                                        p.RowNumber  = jmop.RowNumber and
                                                        p.HallId     = h.HallId
        join                    dbo.Ticket t     on     t.PlaceId       =   p.PlaceId and
                                                        t.ScreeningId   =   sc.ScreeningId

        if (select count(*) from #JsonMakeOrder jmo where jmo.OrderId is not null) != 0
        begin;
            THROW 51000, 'ERROR', 1;
        end;

        insert into             dbo.[Order]
                                (
                                    UserId,
                                    [Date]
                                )
        select                  top 1 jmo.UserId,
                                jmo.CurrentDate
        from                    #JsonMakeOrder jmo
        left join               dbo.[Order] o   on    jmo.CurrentDate = o.[Date] and
                                                      jmo.UserId = o.UserId
        where                   o.OrderId is null

        update                  dbo.[Ticket]
        set                     OrderId = o.OrderId
        from                    #JsonMakeOrder jmo
        join                    dbo.[Order] o    on     o.[Date] = jmo.CurrentDate and
                                                        jmo.UserId = o.UserId
        join                    dbo.Ticket t     on     t.PlaceId       =   jmo.PlaceId and
                                                        t.ScreeningId   =   jmo.ScreeningId
        commit
    end try
    begin catch
        print 'Oops. There were issues when executing [dbo].[usp_ParseJsonMakeOrder]'
        rollback
    end catch
end
go