use CinemaDb
go

if object_id('dbo.usp_ParseJsonAddMovie', 'P') is not null
    drop procedure dbo.usp_ParseJsonAddMovie
go

create procedure dbo.usp_ParseJsonAddMovie
(
    @json nvarchar(max)
)
as
begin
    if dbo.ufn_IsUserAdmin(@json) = 0
        return

    begin try
        begin transaction
            create table #JsonAddMovie
            (
                MovieEIDR               nvarchar(34),
                MovieName               nvarchar(50),
                MovieRating             decimal(3,1),
                MovieReleaseDate        date,
                MovieDuration           time,
                MovieAgeLimit           tinyint,
                Genres                  nvarchar(max),
                Actors                  nvarchar(max),
                Countries               nvarchar(max)
            )

            insert into     #JsonAddMovie
                            (
                                MovieEIDR,
                                MovieName,
                                MovieRating,
                                MovieReleaseDate,
                                MovieDuration,
                                MovieAgeLimit,
                                Genres,
                                Actors,
                                Countries
                            )
            select  jsam.MovieEIDR,
                    jsam.MovieName,
                    jsam.MovieRating,
                    jsam.MovieReleaseDate,
                    jsam.MovieDuration,
                    jsam.MovieAgeLimit,
                    jsam.Genres,
                    jsam.Actors,
                    jsam.Countries
            from openjson(@json)
            with
            (
                MovieEIDR           nvarchar(34)    '$.movie.eidr',
                MovieName           nvarchar(50)    '$.movie.name',
                MovieRating         decimal(3,1)    '$.movie.rating',
                MovieReleaseDate    date            '$.movie.releaseDate',
                MovieDuration       time            '$.movie.duration',
                MovieAgeLimit       tinyint         '$.movie.ageLimit',
                Genres              nvarchar(max)   '$.genre'                   as json,
                Actors              nvarchar(max)   '$.actor'                   as json,
                Countries           nvarchar(max)   '$.country'                 as json
            ) jsam

            declare @movieNumberBeforeInsert int
            select @movieNumberBeforeInsert = count(*)
            from dbo.Movie

            insert into     dbo.Movie
                            (
                                EIDR,
                                [Name],
                                Rating,
                                ReleaseDate,
                                Duration,
                                AgeLimit
                            )
            select  jsam.MovieEIDR,
                    jsam.MovieName,
                    jsam.MovieRating,
                    jsam.MovieReleaseDate,
                    jsam.MovieDuration,
                    jsam.MovieAgeLimit
            from #JsonAddMovie jsam
            left join   dbo.Movie m     on      m.EIDR = jsam.MovieEIDR
            where m.EIDR is null

            declare @movieNumberAfterInsert int
            select @movieNumberAfterInsert = count(*)
            from dbo.Movie

            if @movieNumberBeforeInsert = @movieNumberAfterInsert
                throw 51000, 'Movie EIDR was repeated.', 1

            create table #JsonAddMovieActors
            (
                ActorFirstName  nvarchar(50),
                ActorLastName	nvarchar(50),
                CountryName     nvarchar(50)
            )

            insert into     #JsonAddMovieActors
                            (
                                ActorFirstName,
                                ActorLastName,
                                CountryName
                            )
            select      jsama.ActorFirstName,
                        jsama.ActorLastName,
                        jsama.CountryName
            from #JsonAddMovie jsam
            cross apply openjson(jsam.Actors)
            with
            (
                ActorFirstName  nvarchar(50)    '$.firstName',
                ActorLastName   nvarchar(50)    '$.lastName',
                CountryName     nvarchar(50)    '$.country.name'
            ) jsama

            insert into     dbo.Actor
                            (
                                FirstName,
                                LastName,
                                CountryId
                            )
            select      jsama.ActorFirstName,
                        jsama.ActorLastName,
                        c.CountryId
            from #JsonAddMovieActors jsama
            join        dbo.Country c       on      c.[Name] = jsama.CountryName
            left join   dbo.Actor a         on      a.FirstName = jsama.ActorFirstName
                                            and     a.LastName = jsama.ActorLastName
                                            and     a.CountryId = c.CountryId
            where a.FirstName is null

            insert into     dbo.MovieActorBridge
                            (
                                MovieId,
                                ActorId
                            )
            select      m.MovieId,
                        a.ActorId
            from #JsonAddMovie jsam
            cross join #JsonAddMovieActors jsama
            join        dbo.Movie m                     on      m.EIDR = jsam.MovieEIDR
            join        dbo.Actor a                     on      a.FirstName = jsama.ActorFirstName
                                                        and     a.LastName = jsama.ActorLastName
            left join   dbo.MovieActorBridge mab        on      mab.MovieId = m.MovieId and
                                                                mab.ActorId = a.ActorId
            where   mab.MovieId is null

            insert into     dbo.MovieGenreBridge
                            (
                                MovieId,
                                GenreId
                            )
            select      m.MovieId,
                        g.GenreId
            from #JsonAddMovie jsam
            cross apply openjson(jsam.Genres)
            with
            (
                GenreName   nvarchar(20)    '$.name'
            ) jsamg
            join        dbo.Movie m                 on      m.EIDR = jsam.MovieEIDR
            join        dbo.Genre g                 on      g.Name = jsamg.GenreName
            left join   dbo.MovieGenreBridge mgb    on      mgb.MovieId = m.MovieId and
                                                            mgb.GenreId = g.GenreId
            where   mgb.MovieId is null

            insert into     dbo.MovieCountryBridge
                            (
                                MovieId,
                                CountryId
                            )
            select      m.MovieId,
                        c.CountryId
            from    #JsonAddMovie jsam
            cross apply openjson(jsam.Countries)
            with
            (
                CountryName     nvarchar(50)    '$.name'
            ) jsamc
            join        dbo.Movie m                     on      m.EIDR = jsam.MovieEIDR
            join        dbo.Country c                   on      c.[Name] = jsamc.CountryName
            left join   dbo.MovieCountryBridge mcb      on      mcb.MovieId = m.MovieId and
                                                                mcb.CountryId = c.CountryId
            where mcb.MovieId is null
        commit transaction
    end try
    begin catch
        print 'Adding movie failed.'
        rollback transaction
    end catch
end
go