use CinemaDb;
go

if object_id('dbo.usp_ParseJsonAddRole','P') is not null
	drop procedure dbo.usp_ParseJsonAddRole
go

create procedure dbo.usp_ParseJsonAddRole 
(
	@json nvarchar(max)
)
as
begin
	insert into		dbo.[Role] ([Name])
	select			jr.[Name]
	from			openjson(@json)
					with
					(
						[Name] nvarchar(50) '$.name'
					) jr
	left join		dbo.[Role] r on jr.[Name] = r.[Name]
	where			r.[Name] is null
end
go