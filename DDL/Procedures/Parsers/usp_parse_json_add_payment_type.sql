use CinemaDb
go

if object_id('dbo.usp_ParseJsonAddPaymentType', 'P') is not null
	drop procedure dbo.usp_ParseJsonAddPaymentType
go

create procedure dbo.usp_ParseJsonAddPaymentType
(
	@json nvarchar(max)
)
as
begin
	insert into		dbo.PaymentType ([Name])
	select			jspt.[Name]
	from			openjson(@json)
					with
					(
						[Name] nvarchar(50) '$.name'
					) jspt
	left join		dbo.PaymentType pt on pt.[Name] = jspt.[Name]
	where			pt.[Name] is null
end
go