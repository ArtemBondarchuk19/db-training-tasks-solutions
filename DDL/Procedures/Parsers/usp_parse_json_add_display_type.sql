use CinemaDb
go

if object_id('dbo.usp_ParseJsonAddDisplayType', 'P') is not null
    drop procedure dbo.usp_ParseJsonAddDisplayType
go

create procedure dbo.usp_ParseJsonAddDisplayType
(
    @json nvarchar(max)
)
as
begin
    insert into     dbo.DisplayType ([Name])
    select          jsdt.[Name]
    from            openjson(@json)
                    with
                    (
                        [Name] nvarchar(5) '$.name'
                     ) jsdt
    left join       dbo.DisplayType dt on dt.[Name] = jsdt.[Name]
    where           dt.[Name] is null
end
go