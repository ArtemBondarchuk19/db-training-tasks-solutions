use CinemaDb;
go

if object_id('dbo.usp_ParseJsonAddPlaceType','P') is not null
	drop procedure dbo.usp_ParseJsonAddPlaceType
go

create procedure dbo.usp_ParseJsonAddPlaceType
(
	@json nvarchar(max)
)
as
begin
	insert into			dbo.PlaceType
                        (
                            [Name],
                            Coefficient
                        )
	select				jpt.[Name],
                        jpt.Coefficient
	from				openjson(@json)
						with 
						(
							[Name]		    nvarchar(50)	'$.name',
                            Coefficient     float           '$.coefficient'
						) jpt
	left join			dbo.PlaceType pt on jpt.[Name] = pt.[Name]
	where				pt.[Name] is null
end
go