use CinemaDb
go

if object_id('dbo.usp_ParseJsonAddCountry', 'P') is not null
    drop procedure dbo.usp_ParseJsonAddCountry
go

create proc dbo.usp_ParseJsonAddCountry
(
    @json nvarchar(max)
)
as
begin
    insert into     dbo.Country ([Name])
    select          js_countries.[Name]
    from            openjson(@json)
                    with
                    (
                        [Name] varchar(50) '$.name' 
                    ) js_countries
    left join       dbo.Country on js_countries.[Name] = dbo.Country.[Name]
    where           dbo.Country.[Name] is null
end
go