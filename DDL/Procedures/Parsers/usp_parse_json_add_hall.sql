use CinemaDb
go

if object_id('dbo.usp_ParseJsonAddHall', 'P') is not null
    drop procedure dbo.usp_ParseJsonAddHall
go

create proc dbo.usp_ParseJsonAddHall
(
    @json nvarchar(max)
)
as
begin
    if dbo.ufn_IsUserAdmin(@json) = 0
        return

    begin try
        begin transaction
            create table        #JsonAddHall
            (
                CinemaAddress       nvarchar(200),
                CinemaName          nvarchar(50),
                CityName            nvarchar(50),
                CountryName         nvarchar(50),
                Halls               nvarchar(max)
            )

            insert into         #JsonAddHall
                                (
                                    CinemaAddress,
                                    CinemaName,
                                    CityName,
                                    CountryName,
                                    Halls
                                )
            select              *
            from                openjson(@json)
                                with
                                (
                                    CinemaAddress       nvarchar(200)       '$.cinema.address',
                                    CinemaName          nvarchar(50)        '$.cinema.name',
                                    CityName            nvarchar(50)        '$.cinema.city.name',
                                    CountryName         nvarchar(50)        '$.cinema.city.country.name',
                                    Halls               nvarchar(max)       '$.hall'    as json
                                )

            insert into         dbo.Hall
                                (
                                    [Name],
                                    [Floor],
                                    DefaultTicketPrice,
                                    CinemaId
                                )
            select              jshall.[Name],
                                jshall.[Floor],
                                jshall.DefaultTicketPrice,
                                dbo.Cinema.CinemaId
            from                #JsonAddHall jsah
            join                dbo.Country on jsah.CountryName = dbo.Country.[Name]
            join                dbo.City on jsah.CityName = dbo.City.[Name]
                                and dbo.Country.CountryId = dbo.City.CountryId
            join                dbo.Cinema on jsah.CinemaName = dbo.Cinema.[Name]
                                and jsah.CinemaAddress = dbo.Cinema.[Address]
                                and dbo.City.CityId = dbo.Cinema.CityId
            outer apply         openjson(jsah.Halls)
                                with
                                (
                                    [Name]              nvarchar(50)    '$.name',
                                    [Floor]             tinyint         '$.floor',
                                    DefaultTicketPrice	money           '$.defaultTicketPrice'
                                ) jshall
            left join           dbo.Hall on jshall.[Name] = dbo.Hall.[Name]
                                and dbo.Cinema.CinemaId = dbo.Hall.CinemaId
            where               dbo.Hall.[Name] is null
    commit
    end try
    begin catch
        print 'Halls adding failed'
        rollback
    end catch
end
go