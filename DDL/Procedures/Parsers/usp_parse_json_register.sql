use CinemaDb;
go

if object_id('dbo.usp_ParseJsonRegister','P') is not null
	drop procedure dbo.usp_ParseJsonRegister
go

create procedure dbo.usp_ParseJsonRegister
(
	@json nvarchar(max)
)
as
begin

	  DECLARE @Salt VARCHAR(25);
	  DECLARE @Seed int;
	  DECLARE @LCV tinyint;
	  DECLARE @CTime DATETIME;

	  SET @CTime = GETDATE();
	  SET @Seed = (DATEPART(hh, @Ctime) * 10000000) + (DATEPART(n, @CTime) * 100000)
		  + (DATEPART(s, @CTime) * 1000) + DATEPART(ms, @CTime);
	  SET @LCV = 1;
	  SET @Salt = CHAR(ROUND((RAND(@Seed) * 94.0) + 32, 3));

	  WHILE (@LCV < 25)
	  BEGIN
		SET @Salt = @Salt + CHAR(ROUND((RAND() * 94.0) + 32, 3));
		SET @LCV = @LCV + 1;
	  END;

		insert into dbo.[User]
		(
			Email,
			Phone,
			PasswordHash,
			Salt
		)
		select			jru.Email,
						jru.Phone,
						hashbytes('SHA1', jru.[Password] + @Salt),
						@Salt
		from openjson(@json)
		with
		(
			Email		nvarchar(100)	'$.user.email',
			Phone		nvarchar(25)	'$.user.phone',
			[Password]	nvarchar(64)	'$.user.password'
		) jru
		left join	dbo.[User] u on jru.Email = u.Email
		where		u.Email is null
end
go