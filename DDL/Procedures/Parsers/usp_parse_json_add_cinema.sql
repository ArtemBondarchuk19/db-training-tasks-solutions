use CinemaDb
go

if object_id('dbo.usp_ParseJsonAddCinema', 'P') is not null
    drop procedure dbo.usp_ParseJsonAddCinema
go

create proc dbo.usp_ParseJsonAddCinema
(
    @json nvarchar(max)
)
as
begin

    if dbo.ufn_IsUserAdmin(@json) = 0
        return

    insert into     dbo.Cinema 
                    (
                        [Name],
                        [Address],
                        CityId
                    )
    select          json_cinema.CinemaName,
                    json_cinema.CinemaAddress, 
                    dbo.City.CityId
    from            openjson(@json, '$.cinema')
                    with
                    (
                        CinemaName      varchar(50)     '$.name',
                        CinemaAddress   varchar(200)    '$.address',
                        CityName        varchar(50)     '$.city.name',
                        CountryName     varchar(50)     '$.city.country.name'
                    ) json_cinema
    join            dbo.Country on json_cinema.CountryName = dbo.Country.[Name]
    join            dbo.City on json_cinema.CityName = dbo.City.[Name] 
                    and dbo.Country.CountryId = dbo.City.CountryId
    left join       dbo.Cinema on json_cinema.CinemaName = dbo.Cinema.[Name] 
                    and json_cinema.CinemaAddress = dbo.Cinema.[Address] 
                    and dbo.City.CityId = dbo.Cinema.CityId 
    where           dbo.Cinema.[Name] is null
end
go