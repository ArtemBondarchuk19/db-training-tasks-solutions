use CinemaDb;
go

if object_id('dbo.usp_ParseJsonAddScreeningType','P') is not null
	drop procedure dbo.usp_ParseJsonAddScreeningType
go

create procedure dbo.usp_ParseJsonAddScreeningType
(
	@json nvarchar(max)
)
as
begin
	insert into			dbo.ScreeningType
						(
							[Name],
							[Start],
							[End],
							Coefficient
						)
	select				jst.[Name],
						jst.[Start],
						jst.[End],
						jst.Coefficient
	from				openjson(@json)
						with 
						(
							[Name]			nvarchar(10)		'$.name',
							[Start]			time				'$.start',
							[End]			time				'$.end',
							Coefficient 	decimal(3, 2)		'$.coefficient'
						) jst
	left join			dbo.ScreeningType st on jst.[Name] = st.[Name]
	where				st.[Name] is null
end
go