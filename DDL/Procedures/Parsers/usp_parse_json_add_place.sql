use CinemaDb
go

if object_id('dbo.usp_ParseJsonAddPlace', 'P') is not null
    drop procedure dbo.usp_ParseJsonAddPlace
go

create proc dbo.usp_ParseJsonAddPlace
(
    @json nvarchar(max)
)
as
begin
    if dbo.ufn_IsUserAdmin(@json) = 0
        return

    begin try
        begin transaction
            create table        #JsonAddPlace
            (
                HallName            nvarchar(50),
                CinemaAddress       nvarchar(100),
                CinemaName          nvarchar(50),
                CityName            nvarchar(50),
                CountryName         nvarchar(50),
                Places              nvarchar(max)
            )

            insert into         #JsonAddPlace
                                (
                                    HallName,
                                    CinemaAddress,
                                    CinemaName,
                                    CityName,
                                    CountryName,
                                    Places
                                )
            select              *
            from                openjson(@json)
                                with
                                (
                                    HallName            nvarchar(50)        '$.hall.name    ',
                                    CinemaAddress       nvarchar(100)       '$.hall.cinema.address',
                                    CinemaName          nvarchar(50)        '$.hall.cinema.name',
                                    CityName            nvarchar(50)        '$.hall.cinema.city.name',
                                    CountryName         nvarchar(50)        '$.hall.cinema.city.country.name',
                                    Places              nvarchar(max)       '$.place'    as json
                                )

            insert into         dbo.Place
                                (
                                    RowNumber,
                                    SeatNumber,
                                    PlaceTypeId,
                                    HallId
                                )
            select              jsPlace.RowNumber,
                                jsPlace.SeatNumber,
                                dbo.PlaceType.PlaceTypeId,
                                dbo.Hall.HallId
            from                #JsonAddPlace jsap
            join                dbo.Country on jsap.CountryName = dbo.Country.[Name]
            join                dbo.City on jsap.CityName = dbo.City.[Name]
                                and dbo.Country.CountryId = dbo.City.CountryId
            join                dbo.Cinema on jsap.CinemaName = dbo.Cinema.[Name]
                                and jsap.CinemaAddress = dbo.Cinema.[Address]
                                and dbo.City.CityId = dbo.Cinema.CityId
            join                dbo.Hall on dbo.Cinema.CinemaId = dbo.Hall.CinemaId
                                and jsap.HallName = dbo.Hall.[Name]
            outer apply         openjson(jsap.Places)
                                with
                                (
                                    RowNumber           tinyint         '$.rowNumber',
                                    SeatNumber          tinyint         '$.seatNumber',
                                    PlaceTypeName   	nvarchar(50)    '$.placeType.name'
                                ) jsPlace
            join                dbo.PlaceType on jsPlace.PlaceTypeName = dbo.PlaceType.[Name]
            left join           dbo.Place on jsPlace.RowNumber = dbo.Place.RowNumber
                                and jsPlace.SeatNumber = dbo.Place.SeatNumber
                                and dbo.Hall.HallId = dbo.Place.HallId
            where               dbo.Place.PlaceId is null

		commit
	end try
	begin catch
		print 'Places adding failed'
        rollback
	end catch
end
go