use CinemaDb
go

if object_id('dbo.usp_ParseJsonAddScreening', 'P') is not null
    drop procedure dbo.usp_ParseJsonAddScreening
go

create procedure dbo.usp_ParseJsonAddScreening
(
    @json nvarchar(max)
)
as
begin
    if dbo.ufn_IsUserAdmin(@json) = 0
        return

    begin try
        begin transaction
            create table #JsonAddScreening
            (
                ScreeningStartDate      smalldatetime,
                ScreeningEndDate        smalldatetime,
                MovieId                 uniqueidentifier,
                ScreeningTypeId         tinyint,
                DisplayTypeId           tinyint,
                HallId                  uniqueidentifier,
                CinemaId                uniqueidentifier,
                CityId                  uniqueidentifier,
                CountryId               uniqueidentifier,
            )

            insert into     #JsonAddScreening
                            (
                                ScreeningStartDate,
                                ScreeningEndDate,
                                MovieId,
                                ScreeningTypeId,
                                DisplayTypeId,
                                HallId,
                                CinemaId,
                                CityId,
                                CountryId
                            )
            select  jsas.ScreeningStartDate,
                    cast(cast(jsas.ScreeningStartDate as datetime) + cast(m.Duration as datetime) + cast('00:30:00' as datetime) as smalldatetime),
                    m.MovieId,
                    st.ScreeningTypeId,
                    dt.DisplayTypeId,
                    h.HallId,
                    c3.CinemaId,
                    c2.CityId,
                    c1.CountryId
            from openjson(@json)
                with
                (
                    ScreeningStartDate      smalldatetime       '$.screening.startDate',
                    MovieEIDR               nvarchar(34)        '$.movie.eidr',
                    DisplayTypeName         nvarchar(5)         '$.displayType.name',
                    HallName                nvarchar(50)        '$.hall.name',
                    CinemaName              nvarchar(50)        '$.hall.cinema.name',
                    CinemaAddress           nvarchar(200)       '$.hall.cinema.address',
                    CityName                nvarchar(50)        '$.hall.cinema.city.name',
                    CountryName             nvarchar(50)        '$.hall.cinema.city.country.name'
                ) jsas
                join        dbo.Movie m             on      m.EIDR = jsas.MovieEIDR
                join        dbo.DisplayType dt      on      dt.[Name] = jsas.DisplayTypeName
                join        dbo.Country c1          on      c1.[Name] = jsas.CountryName
                join        dbo.City c2             on      c2.[Name] = jsas.CityName and
                                                            c2.CountryId = c1.CountryId
                join        dbo.Cinema c3           on      c3.[Name] = jsas.CinemaName and
                                                            c3.[Address] = jsas.CinemaAddress and
                                                            c3.CityId = c2.CityId
                join        dbo.Hall h              on      h.[Name] = jsas.HallName and
                                                            h.CinemaId = c3.CinemaId
                join        dbo.ScreeningType st    on      st.ScreeningTypeId = dbo.ufn_GetScreeningTypeId(jsas.ScreeningStartDate)

            if not exists
            (
                select 1
                from #JsonAddScreening jsas
                join dbo.Screening s on jsas.HallId = s.HallId
                where   jsas.ScreeningStartDate <= s.EndDate and
                        jsas.ScreeningEndDate >= s.StartDate
            )
            begin
                insert into     dbo.Screening
                                (
                                    StartDate,
                                    EndDate,
                                    MovieId,
                                    ScreeningTypeId,
                                    DisplayTypeId,
                                    HallId
                                )
                select  jsas.ScreeningStartDate,
                        jsas.ScreeningEndDate,
                        jsas.MovieId,
                        jsas.ScreeningTypeId,
                        jsas.DisplayTypeId,
                        jsas.HallId
                from #JsonAddScreening jsas
                left join   dbo.Screening s             on      s.StartDate = jsas.ScreeningStartDate and
                                                                s.EndDate = jsas.ScreeningEndDate and
                                                                s.HallId = jsas.HallId
                where s.HallId is null

                insert into     dbo.Ticket
                                (
                                    FinalPrice,
                                    ScreeningId,
                                    PlaceId
                                )
                select  FinalPrice = h.DefaultTicketPrice * pt.Coefficient * st.Coefficient,
                        s.ScreeningId,
                        p.PlaceId
                from #JsonAddScreening jsas
                join        dbo.Screening s             on      s.StartDate = jsas.ScreeningStartDate and
                                                                s.EndDate = jsas.ScreeningEndDate and
                                                                s.HallId = jsas.HallId
                join        dbo.Hall h                  on      h.HallId = jsas.HallId
                join        dbo.Place p                 on      p.HallId = h.HallId
                join        dbo.PlaceType pt            on      pt.PlaceTypeId = p.PlaceTypeId
                join        dbo.ScreeningType st        on      st.ScreeningTypeId = jsas.ScreeningTypeId
                left join   dbo.Ticket t                on      t.ScreeningId = s.ScreeningId and
                                                                t.PlaceId = p.PlaceId
                where t.TicketId is null
            end
        commit
        
    end try
    begin catch
        print 'Adding screening failed.'
        rollback
    end catch
end
go