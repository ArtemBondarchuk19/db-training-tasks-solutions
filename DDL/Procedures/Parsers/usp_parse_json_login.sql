use CinemaDb;
go

if object_id('dbo.usp_ParseJsonLogin','P') is not null
    drop procedure dbo.usp_ParseJsonLogin
go

create procedure dbo.usp_ParseJsonLogin
(
    @json nvarchar(max),
    @isLoginSuccess bit output
)
as
begin
    set @isLoginSuccess = 
    case when exists
    (
        select      1
        from        openjson(@json)
                    with
                    (
                        Email nvarchar(100)         '$.user.email',
                        [Password] nvarchar(64)     '$.user.password'
                    )   jl
        join        dbo.[User] u on 
                    (
                        jl.Email = u.Email and
                        hashbytes('SHA1', jl.[Password] + u.Salt) = u.PasswordHash
                    )
    )
    then 1
    else 0
    end
end
go