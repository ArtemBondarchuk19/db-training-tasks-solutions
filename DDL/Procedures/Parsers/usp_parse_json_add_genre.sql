use CinemaDb
go

if object_id('dbo.usp_ParseJsonAddGenre', 'P') is not null
	drop procedure dbo.usp_ParseJsonAddGenre
go

create procedure dbo.usp_ParseJsonAddGenre
(
	@json nvarchar(max)
)
as
begin
	insert into		dbo.Genre ([Name])
	select			jsg.[Name]
	from			openjson(@json)
					with
					(
						[Name] nvarchar(20) '$.name'
					) jsg
	left join		dbo.Genre g on g.[Name] = jsg.[Name]
	where			g.[Name] is null
end
go