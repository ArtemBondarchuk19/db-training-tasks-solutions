use CinemaDb
go

if object_id('dbo.usp_GenerateScreening', 'P') is not null
    drop procedure dbo.usp_GenerateScreening
go

create procedure dbo.usp_GenerateScreening
(
    @screeningNumber int
)
as
begin
    begin try
    truncate table dbo.ScreeningJson

    create table #Screening
    (
        StartDate               smalldatetime           not null,
        MovieId                 uniqueidentifier        not null,
        ScreeningTypeId         tinyint                 not null,
        DisplayTypeId           tinyint                 not null,
        HallId                  uniqueidentifier        not null
    )

    create table #Movie
    (
        RowNumber   int                 not null    identity,
        MovieId     uniqueidentifier    not null,
        RandomId    uniqueidentifier    not null    default     newid()

        constraint PK_#Movie_RowNumber primary key(
            RowNumber
        )
    )

    insert into     #Movie
                    (
                        MovieId
                    )
    select m.MovieId
    from dbo.Movie m

    create table #ScreeningType
    (
        ScreeningTypeId     tinyint             not null,
        RandomId            uniqueidentifier    not null    default     newid()
    )

    insert into     #ScreeningType
                    (
                        ScreeningTypeId
                    )
    select st.ScreeningTypeId
    from dbo.ScreeningType st

    create table #DisplayType
    (
        DisplayTypeId       tinyint             not null,
        RandomId            uniqueidentifier    not null    default     newid()
    )

    insert into     #DisplayType
                    (
                        DisplayTypeId
                    )
    select dt.DisplayTypeId
    from dbo.DisplayType dt

    create table #Hall
    (
        RowNumber   int                 not null    identity,
        HallId      uniqueidentifier    not null,
        RandomId    uniqueidentifier    not null    default     newid()

        constraint PK_#Hall_RowNumber primary key(
            RowNumber
        )
    )

    insert into     #Hall
                    (
                        HallId
                    )
    select h.HallId
    from dbo.Hall h

    declare @counter int = 1
        while @counter <= @screeningNumber
        begin
            truncate table #Screening

            declare @startDate smalldatetime
            declare @date datetime = cast(dateadd(day, rand() * datediff(day, '2000-01-01', getdate()), '2000-01-01') as datetime)
            declare @hours tinyint = dbo.ufn_GetRandomNumber(0, 23)
            declare @minutes tinyint = dbo.ufn_GetRandomNumber(0, 59)
            set @startDate = cast(@date + cast(concat(@hours, ':', @minutes) as datetime) as smalldatetime)

            declare @randomMovieId uniqueidentifier
            declare @randomMovieRowNumber int
            ;with CTE_MovieMinRandomId as
            (
                select min(m.RandomId) MinRandomId
                from #Movie m
            )
            select top 1    @randomMovieId = m.MovieId,
                            @randomMovieRowNumber = m.RowNumber
            from #Movie m
            cross join CTE_MovieMinRandomId cte
            where m.RandomId = cte.MinRandomId

            update #Movie
            set RandomId = newid()
            where RowNumber = @randomMovieRowNumber

            declare @randomScreeningTypeId tinyint
            ;with CTE_ScreeningTypeMinRandomId as
            (
                select min(st.RandomId) MinRandomId
                from #ScreeningType st
            )
            select top 1 @randomScreeningTypeId = st.ScreeningTypeId
            from #ScreeningType st
            cross join CTE_ScreeningTypeMinRandomId cte
            where st.RandomId = cte.MinRandomId

            update #ScreeningType
            set RandomId = newid()
            where ScreeningTypeId = @randomScreeningTypeId

            declare @randomDisplayTypeId tinyint
            ;with CTE_DisplayTypeMinRandomId as
            (
                select min(dt.RandomId) MinRandomId
                from #DisplayType dt
            )
            select top 1 @randomDisplayTypeId = dt.DisplayTypeId
            from #DisplayType dt
            cross join CTE_DisplayTypeMinRandomId cte
            where dt.RandomId = cte.MinRandomId

            update #DisplayType
            set RandomId = newid()
            where DisplayTypeId = @randomDisplayTypeId

            declare @randomHallId uniqueidentifier
            declare @randomHallRowNumber int
            ;with CTE_HallMinRandomId as
            (
                select min(h.RandomId) MinRandomId
                from #Hall h
            )
            select top 1    @randomHallId = h.HallId,
                            @randomHallRowNumber = h.RowNumber
            from #Hall h
            cross join CTE_HallMinRandomId cte
            where h.RandomId = cte.MinRandomId

            update #Hall
            set RandomId = newid()
            where RowNumber = @randomHallRowNumber

            insert into     #Screening
                            (
                                StartDate,
                                MovieId,
                                ScreeningTypeId,
                                DisplayTypeId,
                                HallId
                            )
            select @startDate, @randomMovieId, @randomScreeningTypeId, @randomDisplayTypeId, @randomHallId

            ----------------------------------------------------------------------

            declare @userEmail nvarchar(100)
            select top 1 @userEmail = u.Email
            from dbo.[User] u
            join dbo.[Role] r on r.RoleId = u.RoleId
            where r.[Name] = 'admin'

            declare @json nvarchar(max)

            set @json = 
            (
                select      h.[Name] as 'hall.name',
                            cn.[Name] as 'hall.cinema.name',
                            cn.[Address] as 'hall.cinema.address',
                            ct.[Name] as 'hall.cinema.city.name',
                            ctr.[Name] as 'hall.cinema.city.country.name',
                            m.EIDR as 'movie.eidr',
                            s.StartDate as 'screening.startDate',
                            dt.[Name] as 'displayType.name',
                            @userEmail as 'user.email'
                from #Screening s
                join dbo.DisplayType dt on dt.DisplayTypeId = s.DisplayTypeId
                join dbo.Movie m on m.MovieId = s.MovieId
                join dbo.Hall h on h.HallId = s.HallId
                join dbo.Cinema cn on cn.CinemaId = h.CinemaId
                join dbo.City ct on ct.CityId = cn.CityId
                join dbo.Country ctr on ctr.CountryId = ct.CountryId
                for json path, without_array_wrapper
            )

            insert into     dbo.ScreeningJson
                            (
                                [Data]
                            )
            select @json

        set @counter = @counter + 1
        end
    end try
    begin catch
        print 'Screening generation failed.'
    end catch
end
go