use CinemaDb
go

if object_id('dbo.usp_GenerateMovie', 'P') is not null
    drop procedure dbo.usp_GenerateMovie
go

create procedure dbo.usp_GenerateMovie
(
    @movieNumber int
)
as
begin
    begin try
    truncate table dbo.MovieJson

    create table #Movie
    (
        MovieId             int                 not null    identity,
        EIDR                nvarchar(34)        not null,
        [Name]              nvarchar(50)        not null,
        ReleaseDate         date                not null,
        Duration            time                not null,
        AgeLimit            tinyint             not null,
        Rating              decimal(3,1)        not null

        constraint PK_#Movie_MovieId primary key(
            MovieId
        )
    )

    create table #Genre
    (
        GenreId         uniqueidentifier    not null,
        RandomId        uniqueidentifier    not null    default     newid(),
        IsChosen        bit

        constraint PK_#Genre_GenreId primary key(
            GenreId
        )
    )

    insert into     #Genre
                    (
                        GenreId
                    )
    select g.GenreId
    from dbo.Genre g

    create table #MovieGenreBridge
    (
        MovieId         int                     not null,
        GenreId         uniqueidentifier        not null

        constraint PK_#MovieGenreBridge_MovieId_GenreId primary key(
            MovieId,
            GenreId
        )
    )

    create table #Country
    (
        CountryId       uniqueidentifier    not null,
        RandomId        uniqueidentifier    not null    default     newid(),
        IsChosen        bit

        constraint PK_#Country_CountryId primary key(
            CountryId
        )
    )

    insert into     #Country
                    (
                        CountryId
                    )
    select c.CountryId
    from dbo.Country c

    create table #MovieCountryBridge
    (
        MovieId         int                     not null,
        CountryId       uniqueidentifier        not null

        constraint PK_#MovieCountryBridge_MovieId_CountryId primary key (
            MovieId,
            CountryId
        )
    )

    create table dbo.#Actor
    (
        ActorId         int                 not null    identity,
        FirstName       nvarchar(50)        not null,
        LastName        nvarchar(50)        not null,
        CountryId       uniqueidentifier    not null

        constraint PK_#Actor_ActorId primary key(
            ActorId
        )
    )

    create table #MovieActorBridge
    (
        MovieId         int         not null,
        ActorId         int         not null

        constraint PK_#MovieActorBridge_MovieId_ActorId primary key(
            MovieId,
            ActorId
        )
    )

    declare @counter int = 1
        while @counter <= @movieNumber
        begin
            truncate table #Movie

            declare @eidr nvarchar(34)
            declare @eidrPrefix nvarchar(8) = '10.5240/'
            declare @eidrFirstTetrad nvarchar(4) = dbo.ufn_GetRandomAlphaNumericString(4, 4)
            declare @eidrSecondTetrad nvarchar(4) = dbo.ufn_GetRandomAlphaNumericString(4, 4)
            declare @eidrThirdTetrad nvarchar(4) = dbo.ufn_GetRandomAlphaNumericString(4, 4)
            declare @eidrFourthTetrad nvarchar(4) = dbo.ufn_GetRandomAlphaNumericString(4, 4)
            declare @eidrFifthTetrad nvarchar(4) = dbo.ufn_GetRandomAlphaNumericString(4, 4)
            declare @eidrCheckCharacter char = char(dbo.ufn_GetRandomNumber(65, 90))

            set @eidr =
                concat(@eidrPrefix, @eidrFirstTetrad, '-', @eidrSecondTetrad, '-', @eidrThirdTetrad, '-', @eidrFourthTetrad, '-', @eidrFifthTetrad, '-', @eidrCheckCharacter)

            declare @name nvarchar(50) = dbo.ufn_GetRandomAlphaNumericString(2, 50)

            declare @releaseDate date = cast(dateadd(day, rand() * datediff(day, '2000-01-01', getdate()), '2000-01-01') as date)

            declare @duration time
            declare @durationHours tinyint = floor(abs(0.5 * sqrt(-2 * log(rand())) * cos(2 * acos(-1.) * (rand())) + 2))
            if @durationHours = 0
                set @durationHours = 1

            declare @durationMinutes tinyint = dbo.ufn_GetRandomNumber(0, 59)

            set @duration = cast(concat(@durationHours, ':', @durationMinutes) as time)

            declare @ageLimit tinyint = floor(abs((5 * sqrt(-2 * log(rand())) * cos(2 * acos(-1.) * (rand()))) + 18))

            declare @rating decimal(3,1) = cast(dbo.ufn_GetRandomNumber(0, 10) as decimal(3,1))
            if @rating != 10.0
            begin
                declare @tenth decimal(2,1) = dbo.ufn_GetRandomNumber(0, 9) / 10.0
                set @rating = @rating + @tenth
            end

            insert into     #Movie
                            (
                                EIDR,
                                [Name],
                                ReleaseDate,
                                Duration,
                                AgeLimit,
                                Rating
                            )
            select @eidr, @name, @releaseDate, @duration, @ageLimit, @rating

            update #Genre
            set IsChosen = 0

            truncate table #MovieGenreBridge

            declare @movieId int
            select top 1 @movieId = m.MovieId
            from #Movie m

            declare @genreTotalNumber tinyint
            select @genreTotalNumber = count(*)
            from dbo.Genre

            declare @genreNumber tinyint = dbo.ufn_GetRandomNumber(1, @genreTotalNumber)
            declare @genreCounter tinyint = 1
            while @genreCounter <= @genreNumber
            begin
                declare @randomGenreId uniqueidentifier
                ;with CTE_GenreMinRandomId as
                (
                    select min(g.RandomId) MinRandomId
                    from #Genre g
                    where g.IsChosen = 0
                )
                select top 1 @randomGenreId = g.GenreId
                from #Genre g
                cross join CTE_GenreMinRandomId cte
                where g.RandomId = cte.MinRandomId

                update #Genre
                set IsChosen = 1
                where GenreId = @randomGenreId

                insert into     #MovieGenreBridge
                                (
                                    MovieId,
                                    GenreId
                                )
                select @movieId, @randomGenreId

                set @genreCounter = @genreCounter + 1
            end

            update #Country
            set IsChosen = 0

            truncate table #MovieCountryBridge

            declare @countryTotalNumber smallint
            select @countryTotalNumber = count(*)
            from dbo.Country

            declare @countryNumber smallint = dbo.ufn_GetRandomNumber(1, @countryTotalNumber)
            declare @countryCounter tinyint = 1
            while @countryCounter <= @countryNumber
            begin
                declare @randomCountryId uniqueidentifier
                ;with CTE_CountryMinRandomId as
                (
                    select min(c.RandomId) MinRandomId
                    from #Country c
                    where c.IsChosen = 0
                )
                select @randomCountryId = c.CountryId
                from #Country c
                cross join CTE_CountryMinRandomId cte
                where c.RandomId = cte.MinRandomId

                update #Country
                set IsChosen = 1
                where CountryId = @randomCountryId

                insert into     #MovieCountryBridge
                                (
                                    MovieId,
                                    CountryId
                                )
                select @movieId, @randomCountryId

                set @countryCounter = @countryCounter + 1
            end

            truncate table #Actor

            update #Country
            set IsChosen = 0

            declare @actorNumber tinyint = dbo.ufn_GetRandomNumber(1, 20)
            declare @actorCounter tinyint = 1
            while @actorCounter <= @actorNumber
            begin
                declare @firstName nvarchar(50) = dbo.ufn_GetRandomAlphaNumericString(2, 50)
                declare @lastName nvarchar(50) = dbo.ufn_GetRandomAlphaNumericString(2, 50)
                declare @countryId uniqueidentifier
                ;with CTE_CountryMinRandomId as
                (
                    select min(c.RandomId) MinRandomId
                    from #Country c
                    where c.IsChosen = 0
                )
                select top 1 @countryId = c.CountryId
                from #Country c
                cross join CTE_CountryMinRandomId cte
                where c.RandomId = cte.MinRandomId

                update #Country
                set IsChosen = 1
                where CountryId = @countryId

                insert into     #Actor
                                (
                                    FirstName,
                                    LastName,
                                    CountryId
                                )
                select @firstName, @lastName, @countryId

                set @actorCounter = @actorCounter + 1
            end

            truncate table #MovieActorBridge

            insert into     #MovieActorBridge
                            (
                                MovieId,
                                ActorId
                            )
            select @movieId, a.ActorId
            from #Actor a

            ----------------------------------------------------------------------

            declare @userEmail nvarchar(100)
            select top 1 @userEmail = u.Email
            from dbo.[User] u
            join dbo.[Role] r on r.RoleId = u.RoleId
            where r.[Name] = 'admin'

            declare @json nvarchar(max)

            set @json = 
            (
                select      m.EIDR as 'movie.eidr',
                            m.[Name] as 'movie.name',
                            m.ReleaseDate as 'movie.releaseDate',
                            m.Duration as 'movie.duration',
                            m.AgeLimit as 'movie.ageLimit',
                            m.Rating as 'movie.rating',
                            (
                                select g.[Name] as 'name'
                                from dbo.Genre g
                                join #MovieGenreBridge mgb on mgb.GenreId = g.GenreId
                                where mgb.MovieId = m.MovieId
                                for json path
                            ) as 'genre',
                            (
                                select c.[Name] as 'name'
                                from dbo.Country c
                                join #MovieCountryBridge mcb on mcb.CountryId = c.CountryId
                                where mcb.MovieId = m.MovieId
                                for json path
                            ) as 'country',
                            (
                                select  a.FirstName as 'firstName',
                                        a.LastName as 'lastName',
                                        c.[Name] as 'country.name'
                                from #Actor a
                                join #MovieActorBridge mab on mab.ActorId = a.ActorId
                                join dbo.Country c on c.CountryId = a.CountryId
                                where mab.MovieId = m.MovieId
                                for json path
                            ) as 'actor',
                            @userEmail as 'user.email'
                from #Movie m
                for json path, without_array_wrapper
            )

            insert into     dbo.MovieJson
                            (
                                [Data]
                            )
            select @json

        set @counter = @counter + 1
        end
    end try
    begin catch
        print 'Movie generation failed.'
    end catch
end
go