﻿use CinemaDb
go

if object_id('dbo.usp_GenerateCinema', 'P') is not null
    drop procedure dbo.usp_GenerateCinema
go

create proc dbo.usp_GenerateCinema
(
    @cinemaCount            int,                --number of cinemas to generate
    @capitalMultiplier      tinyint = 3,        --cinemas ratio for capitals
    @userEmail              nvarchar(50)        --admin user email
)
as
begin
    begin try
        begin transaction

            create table        #fetchCityTable
            (
                Id      int               not null  identity  primary key,
                CityId  uniqueidentifier  not null  unique
            )

            create table        #res
            (
                CinemaName          nvarchar(50),
                CinemaAddress       nvarchar(200),
                CityId              uniqueidentifier
            )

            create table        #capitalsTable
            (
                CityId          uniqueidentifier    not null    primary key,
                RandomId        uniqueidentifier
            )

            insert into         #capitalsTable (CityId)
            select              *
            from                dbo.ufn_GetCapitals()

            insert into         #fetchCityTable
                                (
                                    CityId
                                )
            select              CityId
            from                dbo.City

            declare @citiesCount int
            declare @non_capitals int
            declare @capitals smallint

            select              @citiesCount=count(*)
            from                dbo.City

            select              @capitals=count(*)
            from                #capitalsTable

            set @non_capitals = @citiesCount - @capitals
            declare @perCity int = @cinemaCount / (@capitalMultiplier * @capitals + @non_capitals)
            declare @remainder int = @cinemaCount - @perCity * (@capitalMultiplier * @capitals + @non_capitals)

            declare     @cityId             uniqueidentifier
            declare     @i                  int
            declare     @j                  int = 1
            declare     @flag               int
            declare     @iterCount          int
            declare     @cinemaName         nvarchar(50)
            declare     @cinemaAddress      nvarchar(200)

            while 1 = 1
            begin
                set @flag = 0
                select              @flag = Id,
                                    @cityId = CityId
                from                #fetchCityTable
                where               Id = @j

                if (@flag = 0)
                    break

                if (@cityId in (select CityId from #capitalsTable))
                    set @iterCount = @capitalMultiplier * @perCity
                else
                    set @iterCount = @perCity

                set @i = 0
                while (@i < @iterCount)
                    begin
                        exec dbo.usp_GenerateRandomString @MinLength = 4, @MaxLength = 20, @Result = @cinemaName out
                        exec dbo.usp_GenerateRandomString @MinLength = 15, @MaxLength = 40, @Result = @cinemaAddress out

                        insert into     #res
                        values
                        (
                            upper(left(@cinemaName,1))+substring(@cinemaName,2,len(@cinemaName)),
                            @cinemaAddress,
                            @cityId
                        )

                        set @i = @i + 1
                     end

                 set @j = @j + 1
            end

            set @i = 0
            while (@i < @remainder)
            begin
                exec dbo.usp_GenerateRandomString @MinLength = 4, @MaxLength = 20, @Result = @cinemaName out
                exec dbo.usp_GenerateRandomString @MinLength = 15, @MaxLength = 40, @Result = @cinemaAddress out

                update          #capitalsTable
                set             RandomId = newid()

                ;with CTE_min_randomId(MinRandomId) as
                (
                    select          min(RandomId)
                    from            #capitalsTable
                )
                select top 1    @cityId = CityId
                from            #capitalsTable ct
                join            CTE_min_randomId on MinRandomId = ct.RandomId

                insert into     #res
                values
                (
                    @cinemaName,
                    @cinemaAddress,
                    @cityId
                )

                set @i = @i + 1
            end

            declare @json nvarchar(max)
            set @json = 
            (
                 select         @userEmail as 'user.email',
                                (
                                    select          CinemaAddress as 'address',
                                                    CinemaName as 'name',
                                                    ci.[Name] as 'city.name',
                                                    co.[Name] as 'city.country.name'
                                    from            #res
                                    join            dbo.City ci on #res.CityId = ci.CityId
                                    join            dbo.Country co on ci.CountryId = co.CountryId
                                    for json path
                                ) as 'cinema'
                for json path, without_array_wrapper
            )

            insert into     dbo.CinemaJson
                            (
                                [Data]
                            )
            values          (@json)

        commit
    end try
    begin catch
        print 'Error while executing usp_AddCinemaGenerator... ' + convert(varchar, error_number()) + ' : ' + error_message()
        rollback
    end catch
end
go