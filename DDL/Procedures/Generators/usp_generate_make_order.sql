use CinemaDb;
go

if object_id('dbo.usp_GenerateMakeOrder','P') is not null
	drop procedure dbo.usp_GenerateMakeOrder
go
create procedure dbo.usp_GenerateMakeOrder
(
    @AmountOrders int = 1
)
as
begin
    begin try
        begin tran

               create table #places
               (
                    RowNumber        int not null,
                    SeatNumber       int not null
               )

               create table #PreparedData 
               (
                    HallName       nvarchar(50),
                    ScreeningId    uniqueidentifier,
                    HallId         uniqueidentifier,
                    StartScreening datetime,
                    EndScreening   datetime,
                    CinemaName     nvarchar(50),
                    CinemaAddress  nvarchar(100),
                    CityName       nvarchar(50),
                    CountryName    nvarchar(50)
               )

               insert into  #PreparedData --(ScreeningsData)
                            (
                                HallName,
                                ScreeningId,
                                h.HallId,
                                StartScreening,
                                EndScreening,
                                CinemaName,
                                CinemaAddress,
                                CityName,
                                CountryName
                            )
               select       h.[Name],
                            sc.ScreeningId,
                            sc.HallId,
                            sc.StartDate,
                            sc.EndDate,
                            cm.[Name],
                            cm.[Address],
                            c.[Name],
                            ctr.[Name]
               from         Screening sc
               inner join   dbo.Hall h      on h.HallId      = sc.HallId
               inner join   dbo.Cinema cm   on cm.CinemaId   = h.CinemaId
               inner join   dbo.City c      on c.CityId      = cm.CityId
               inner join   dbo.Country ctr on ctr.CountryId = c.CountryId

               create table #AvailablePlaces
               (
                    ScreeningId     uniqueidentifier,
                    PlaceId         uniqueidentifier,
                    RowNumber       int,
                    SeatNumber      int,
               )
               insert into    #AvailablePlaces
                              (
                                    ScreeningId,
                                    PlaceId,
                                    RowNumber,
                                    SeatNumber
                              )
               select         pd.ScreeningId,
                              t.PlaceId,
                              p.RowNumber,
                              p.SeatNumber
               from           #PreparedData pd
               inner join     dbo.Ticket t on pd.ScreeningId = t.ScreeningId and t.OrderId is null
               inner join     dbo.Place p on t.PlaceId = p.PlaceId

               create table #users
               (
                    RandomId    uniqueidentifier,
                    UserId      uniqueidentifier    not null,
                    Email       nvarchar(100)       not null
               )

               insert into      #users
                                (
                                    UserId,
                                    Email
                                )
               select           UserId,
                                Email
               from             dbo.[User]
               where            RoleId = 1

               declare @counter int = 0
               declare @i tinyint
               declare @json nvarchar(max)
               declare @userEmail nvarchar(100)
               declare @placeInOrder tinyint

               declare @PlaceId uniqueidentifier
               declare @ScreeningId uniqueidentifier
               declare @HallName nvarchar(50)
               declare @StartScreening datetime
               declare @EndScreening datetime
               declare @CinemaName nvarchar(50)
               declare @CinemaAddress nvarchar(100)
               declare @CityName nvarchar(50)
               declare @CountryName nvarchar(50)

               declare @rowNumber int
               declare @seatNumber int

               while @counter < @AmountOrders
               begin

                    update          #users
                    set             RandomId = newid()

                    ;with CTE_min_randomId(MinRandomId) as
                    (
                        select          min(RandomId)
                        from            #users
                    )
                    select top 1    @userEmail = u.Email
                    from            #users u
                    join            CTE_min_randomId on MinRandomId = u.RandomId

                    select          top 1
                                    @ScreeningId = pd.ScreeningId,
                                    @HallName = pd.HallName,
                                    @CinemaAddress = pd.CinemaAddress,
                                    @CinemaName = pd.CinemaName,
                                    @CityName = pd.CityName,
                                    @CountryName =pd.CountryName,
                                    @StartScreening = pd.StartScreening,
                                    @EndScreening = pd.EndScreening
                    from            #PreparedData pd
                    order by        NEWID() --!!!!!!!!

                    set @placeInOrder = dbo.ufn_RandNormal(2, 1, rand())
                    set @i = 0
                    while(@i < @placeInOrder)
                    begin
                        select          top 1
                                        @PlaceId = ap.PlaceId,
                                        @rowNumber = ap.RowNumber,
                                        @seatNumber = ap.SeatNumber
                        from            #AvailablePlaces ap
                        where           ap.ScreeningId = @ScreeningId
                        order by        newid()        --!!!!!!!!!!!


                        insert into     #places
                        values (@rowNumber, @seatNumber)

                        delete from     #AvailablePlaces
                        where           ScreeningId = @ScreeningId and
                                        PlaceId = @PlaceId

                        set @i = @i + 1
                    end

                    set @json =
                    (
                        select
                                        @userEmail          as [user.email],
                                        @CinemaAddress      as [cinema.address],
                                        @CinemaName         as [cinema.name],
                                        @CityName           as [cinema.city.name],
                                        @CountryName        as [cinema.city.country.name],
                                        @StartScreening     as [screening.startDate],
                                        @EndScreening       as [screening.endDate],
                                        @HallName           as [screening.hall.name],
                                        (
                                            select          p.RowNumber as [rowNumber],
                                                            p.SeatNumber as [seatNumber]
                                            from            #places p
                                            for json path
                                        ) as [place]
                        for json path, without_array_wrapper
                    )

                    insert into dbo.Make_order_json([Data]) values (@json)

                    truncate table #places

                    set @counter = @counter + 1
               end
        commit
    end try
    begin catch
        print 'Error while executing usp_MakeOrder... ' + convert(varchar, error_number()) + ' : ' + error_message()
        rollback
    end catch
end
go