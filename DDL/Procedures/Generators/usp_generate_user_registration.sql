use CinemaDb;
go

if object_id('dbo.usp_GenerateUserRegistration','P') is not null
	drop procedure dbo.usp_GenerateUserRegistration
go

create procedure dbo.usp_GenerateUserRegistration
(
    @AmountUser int = 1
)
as
begin
    begin try
        begin tran
            create table #UserTable
            (
                phone         nvarchar(25), 
                [password]    nvarchar(32), 
                email         nvarchar(100)
             )

            declare @LoopCount      int = 0
            declare @Phone          nvarchar(25)
            declare @Password       nvarchar(32)
            declare @Name           nvarchar(50)
            declare @Domain         nvarchar(24)
            declare @SubDomain      nvarchar(16)
            declare @Email          nvarchar(100)
            declare @Json           nvarchar(max)

            while(@LoopCount < @AmountUser)
            begin
                exec dbo.usp_GenerateRandomString @MinLength = 32,@MaxLength = 32, @Result = @Password out
                exec dbo.usp_GenerateRandomString @MinLength = 10,@MaxLength = 40, @Result = @Name out
                exec dbo.usp_GenerateRandomString @MinLength = 5,@MaxLength = 18, @Result = @Domain out
                exec dbo.usp_GenerateRandomString @MinLength = 2,@MaxLength = 16, @Result = @SubDomain out
                exec dbo.usp_GenerateRandomString @MinLength = 2,@MaxLength = 16, @CharPool = '0123456789', @Result = @Phone out
                set @Email = @Name + '@' + @Domain + '.' + @SubDomain
                set @Json = 
                        (
                            select      @Email      as [user.email],
                                        @Password   as [user.password],
                                        @Phone      as [user.phone] 
                            for json path, without_array_wrapper 
                        )
                insert into dbo.RegisterJson Values(@Json)
                set @LoopCount = @LoopCount + 1
            end
        commit
    end try
    begin catch
        print 'Oops. There were issues when executing [dbo].[usp_GenerateUserRegistration]'
        rollback
    end catch
end
go