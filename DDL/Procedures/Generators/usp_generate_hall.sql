use CinemaDb
go

if object_id('dbo.usp_GenerateHall', 'P') is not null
    drop procedure dbo.usp_GenerateHall
go

create proc dbo.usp_GenerateHall
(
    @avgCount               tinyint = 2,    --average count of hall per cinema in provinces
    @avgCountCapital        tinyint = 3,    --average count of hall per cinema in capitals
    @avgPrice               money = 400,    --average default price for provinces
    @avgPriceCapital        money = 650,    --average default price for capitals
    @userEmail              nvarchar(50)    --admin user email
)
as
begin
    begin try
        begin transaction

            create table            #fetchCinemaTable
            (
                Id              int                 not null    identity    primary key,
                CinemaName      nvarchar(50)        not null,
                CinemaAddress   nvarchar(200)       not null,
                CityId          uniqueidentifier    not null,
                CityName        nvarchar(50)        not null,
                CountryName     nvarchar(50)        not null
            )

            create table            #halls
            (
                [Name]              nvarchar(50),
                [Floor]             tinyint,
                [DefaultPrice]      money
            )

            create table    #capitalsTable
            (
                CityId      uniqueidentifier    not null    primary key
            )

            insert into         #capitalsTable
            select              *
            from                dbo.ufn_GetCapitals()

            insert into         #fetchCinemaTable
                                (
                                    CinemaName,
                                    CinemaAddress,
                                    CityId,
                                    CityName,
                                    CountryName
                                )
            select              cin.[Name],
                                cin.[Address],
                                cin.CityId,
                                ci.[Name],
                                co.[Name]
            from                dbo.Cinema cin
            join                dbo.City ci on cin.CityId = ci.CityId
            join                dbo.Country co on ci.CountryId = co.CountryId


            declare @cinemaName nvarchar(50)
            declare @cinemaAddress nvarchar(200)
            declare @cityName nvarchar(50)
            declare @cityId uniqueidentifier
            declare @countryName nvarchar(50)
            declare @hallsCount tinyint
            declare @seed_price money
            declare @hallName nvarchar(50)
            declare @i tinyint
            declare @j int = 1
            declare @flag int
            declare @json nvarchar(max)

            while 1 = 1
            begin
                set @flag = 0
                select              @flag = Id,
                                    @cinemaName = CinemaName,
                                    @cinemaAddress = CinemaAddress,
                                    @cityId = CityId,
                                    @cityName = CityName,
                                    @countryName = CountryName
                from                #fetchCinemaTable
                where               Id = @j

                if (@flag = 0)
                    break

                if (@cityId in (select * from #capitalsTable))
                    begin
                        set @hallsCount = dbo.ufn_RandNormal(@avgCountCapital, 1, rand())
                        set @seed_price = @avgPriceCapital
                    end
                else
                    begin
                        set @hallsCount = dbo.ufn_RandNormal(@avgCount, 1, rand())
                        set @seed_price = @avgPrice
                    end

                set @i = 1
                while (@i <= @hallsCount)
                    begin
                        exec dbo.usp_GenerateRandomString @MinLength = 5, @MaxLength = 10, @Result = @hallName out
                        insert into     #halls
                        values
                        (
                            @hallName,
                            @i,
                            dbo.ufn_RandNormalDecimal(@seed_price, 50, rand())
                        )
                        set @i = @i + 1
                    end

                set @json =
                (
                    select              @userEmail as 'user.email',
                                        @cinemaAddress as 'cinema.address',
                                        @cinemaName as 'cinema.name',
                                        @cityName as 'cinema.city.name',
                                        @countryName as 'cinema.city.country.name',
                                        (
                                            select          h.[Name] as 'name',
                                                            h.[Floor] as 'floor',
                                                            h.DefaultPrice as 'defaultTicketPrice'
                                            from            #halls h
                                            for json path
                                        ) as 'hall'
                    for json path, without_array_wrapper
                )

                insert into     dbo.HallJson
                                (
                                    [Data]
                                )
                values          (@json)

                truncate table #halls
                set @j = @j + 1
            end

        commit
    end try
    begin catch
        print 'Error while executing usp_AddHallGenerator... ' + convert(varchar, error_number()) + ' : ' + error_message()
        rollback
    end catch
end
go