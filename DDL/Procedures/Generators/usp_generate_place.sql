use CinemaDb
go

if object_id('dbo.usp_GeneratePlace', 'P') is not null
    drop procedure dbo.usp_GeneratePlace
go

create proc dbo.usp_GeneratePlace
(
	@rowCount					smallint,
	@seatCount					smallint,
	@userEmail					nvarchar(50)
)
as
begin
    begin try
        begin transaction

		declare @currentHallId  uniqueidentifier
		declare @tmpSeatCount   smallint
		declare @tmpRowCount    smallint
		declare @json			nvarchar(max)
		declare @res		    int

		create table #AddPlaceHallGen
		(
			HallId				uniqueidentifier	not null,
			[Name]				nvarchar(50)		not null,
			CinemaId			uniqueidentifier	not null,
		)

		insert into  #AddPlaceHallGen
		(
			HallId,
			[Name],
			CinemaId
		)
		select 
			HallId,
			[Name],
			CinemaId
		from dbo.Hall

		create table #UnaddedPlaces
		(
			RowNumber			smallint,
			SeatNumber			smallint,
			PlaceTypeId			smallint
		)

		while exists (select 1 from #AddPlaceHallGen)
		begin
		set @tmpRowCount   = @rowCount

			select top 1 
			@currentHallId = phg.HallId 
			from #AddPlaceHallGen phg

			while @tmpRowCount > 0
			begin
				set @tmpSeatCount = @seatCount
				while @tmpSeatCount > 0
				begin
					exec dbo.usp_rand_normal
					@Mean = 4, 
					@StDev = 4, 
					@Result = @res output;

					insert into #UnaddedPlaces
					(
						RowNumber,
						SeatNumber,
						PlaceTypeId
					)
					select 
					RowNumber   = @rowCount  - @tmpRowCount + 1,
					SeatNumber  = @seatCount - @tmpSeatCount + 1,
					PlaceTypeId = @res % 7 + 1

					set @tmpSeatCount = @tmpSeatCount - 1
				end
				set @tmpRowCount	  = @tmpRowCount  - 1
			end

			set @json = (
			select top 1
			[hall.name]						= pg.[Name],
			[hall.cinema.address]			= cnm.[Address],
			[hall.cinema.name]				= cnm.[Name],
			[hall.cinema.city.name]			= ct.[Name],
			[hall.cinema.city.country.name] = cntr.[Name],
				   (
					   select
					   [rowNumber]			= up.RowNumber,
					   [seatNumber]			= up.SeatNumber,
					   [placeType.name]		= 
					   (
						   select pt.[Name]
						   from dbo.PlaceType pt
						   where pt.PlaceTypeId = up.PlaceTypeId
					   )
					   from	#UnaddedPlaces up
					   for json path
				   ) as 'place',
				   [user.email] = @userEmail
			from #AddPlaceHallGen pg
			join dbo.Cinema cnm   on pg.CinemaId   = cnm.CinemaId
			join dbo.City ct	  on cnm.CityId    = ct.CityId
			join dbo.Country cntr on ct.CountryId  = cntr.CountryId
			for json path, without_array_wrapper
			)
			
			truncate table #UnaddedPlaces

			delete from #AddPlaceHallGen
			where HallId = @currentHallId

			insert into dbo.PlaceJson
            (
				[Data]
            )
            values      (@json)
		end

        commit
    end try
    begin catch
        print 'Error while executing usp_AddPlaceGenerator...\n' + convert(varchar, error_number()) + ' : ' + error_message()
        rollback
    end catch
end
go