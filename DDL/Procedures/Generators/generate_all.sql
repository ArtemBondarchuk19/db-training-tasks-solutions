Use CinemaDb
go

exec usp_GenerateUserRegistration @AmountUser = 10000
select count(*) [register jsons] from dbo.RegisterJson
exec usp_LoadData @table = N'RegisterJson ', @parser = N'usp_ParseJsonRegister',@schema = 'dbo'
select count(*) [user] from dbo.[User]
go

exec usp_GenerateMovie @MovieNumber = 10000
select count(*) [movie jsons] from dbo.MovieJson
exec usp_LoadData @table = N'MovieJson', @parser = N'usp_ParseJsonAddMovie',@schema = 'dbo'
select count(*) [movie] from Movie
go

exec usp_GenerateCinema @CinemaCount = 5000,@UserEmail = N'test@test.com'
select count(*) [cinema jsons] from dbo.CinemaJson
exec usp_LoadData @table = N'CinemaJson', @parser = N'usp_ParseJsonAddCinema', @schema = 'dbo'
select count(*) [cinema] from Cinema
go

exec usp_GenerateHall @UserEmail = N'test@test.com'
select count(*) [add hall jsons] from dbo.HallJson
exec usp_LoadData @table = N'HallJson', @parser = N'usp_ParseJsonAddHall', @schema = 'dbo'
select count(*) [hall] from Hall
go

exec usp_GeneratePlace @rowcount = 20,@seatCount = 20, @userEmail = N'test@test.com'
select count(*) [add place jsons] from dbo.PlaceJson
exec usp_LoadData @table = N'PlaceJson', @parser = N'usp_ParseJsonAddPlace', @schema = 'dbo'
select count(*) [place] from Place
go

exec usp_GenerateScreening @screeningNumber = 2000
select count(*) [add screening jsons] from dbo.ScreeningJson
exec usp_LoadData @table = N'ScreeningJson', @parser = N'usp_ParseJsonAddScreening', @schema = 'dbo'
select count(*) [screening] from Screening
go

exec usp_GenerateMakeOrder @AmountOrders = 100
select count(*) [make order jsons] from dbo.Make_order_json
exec usp_LoadData @Table = N'Make_order_json', @parser = N'usp_ParseJsonMakeOrder', @schema = 'dbo'
select count (*) [Order] from [Order]
go


select count(*) from Payment

select count(*) from [Order] o
where o.PaymentId is not null
