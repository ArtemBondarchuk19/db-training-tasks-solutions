use CinemaDb;
go

if object_id('dbo.usp_rand_normal_decimal', 'P') is not null
    drop procedure dbo.usp_rand_normal_decimal
go

create proc dbo.usp_rand_normal_decimal
(
    @Mean float,
    @StDev float,
    @Result float out
)
as
begin
    declare @res float = 0
    while (@res <= 0)
    begin
        set @res = (@StDev * SQRT(-2 * LOG(RAND()))*COS(2*ACOS(-1.)*RAND())) + @Mean
    end
    set @Result = round(@res, 2)
end
go