use CinemaDb;
go

if object_id('dbo.usp_GenerateRandomString','P') is not null
	drop procedure dbo.usp_GenerateRandomString
go

create procedure dbo.usp_GenerateRandomString
(
    @MinLength int = 1,
    @MaxLength int = 255,
    @Result nvarchar(255) out,
    @CharPool nvarchar(255) = 'abcdefghijklmnopqrstuvwxyz'
)
as
begin
declare @Length int
declare @PoolLength int
declare @LoopCount int

set @Length = RAND() * (@MaxLength - @MinLength +1) +@MinLength
set @PoolLength = Len(@CharPool)
set @LoopCount = 0
set @Result = ''

while (@LoopCount < @Length) 
begin
    select @Result = @Result + 
        SUBSTRING(@Charpool, CONVERT(int, RAND() * @PoolLength) + 1, 1)
    select @LoopCount = @LoopCount + 1
end
end
go
