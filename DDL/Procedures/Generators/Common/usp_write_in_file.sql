use CinemaDb;
go

if object_id('dbo.usp_WriteInFile','P') is not null
	drop procedure dbo.usp_WriteInFile
go

create procedure dbo.usp_WriteInFile
(
    @FilePath   nvarchar(250),
    @Query      nvarchar(max)
)
as
begin
    exec master.dbo.sp_configure 'show advanced options',1
        reconfigure with override;
    exec master.dbo.sp_configure 'xp_cmdshell',1
    reconfigure with override;
    declare @command varchar(2000) = ' bcp "' + replace(replace(@Query, char(10), ''), char(13), '') + '" queryout ' + '"' + @FilePath + '"' + ' -w -T'
    print @command
    exec xp_cmdshell @command;
end
go

--exec dbo.usp_WriteInFile @FilePath = 'C:\Users\User\Desktop\�����\ISsoft\Trainings\Database\db-training-tasks-solutions\DDL\Generators\test.txt',
--@Query = 'use CinemaDb;
--select  * 
--from    dbo.[User]'
--go
