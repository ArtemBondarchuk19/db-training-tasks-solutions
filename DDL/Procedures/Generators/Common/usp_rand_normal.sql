use CinemaDb;
go

if object_id('dbo.usp_rand_normal', 'P') is not null
    drop procedure dbo.usp_rand_normal
go

create proc dbo.usp_rand_normal
(
    @Mean int,
    @StDev int,
    @Result int out
)
as
begin
    declare @res int = 0
    while (@res <= 0)
    begin
        set @res = cast(round((@StDev * SQRT(-2 * LOG(RAND()))*COS(2*ACOS(-1.)*RAND())) + @Mean, 0) as int)
    end
    set @Result = @res
end
go