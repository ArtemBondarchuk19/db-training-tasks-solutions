use CinemaDb;
go

if object_id('dbo.usp_ReadFromFile', 'P') is not null
    drop procedure dbo.usp_ReadFromFile
go

create procedure dbo.usp_ReadFromFile
(
    @FilePath nvarchar(250),
    @Result   nvarchar(max) out
)
as
begin
    declare @query nvarchar(500) = N'
    select          @Result =  Stuff(BulkColumn, 1, 1, '''') 
    from            openrowset(BULK ''{FilePath}'', SINGLE_BLOB) as j'

    set @query = replace(@query,'{FilePath}',@FilePath)
    print @query
    exec sp_executesql @query, N'@Result nvarchar(max) out',@Result = @Result
    print @Result
end;
go

--declare @Result nvarchar(max)
--exec usp_ReadFromFile @FilePath = N'C:\Users\User\Desktop\�����\ISsoft\Trainings\Database\db-training-tasks-solutions\DDL\Generators\test.json', @Result = @Result out

--exec usp_ParseJsonRegister @json = @Result
--go

