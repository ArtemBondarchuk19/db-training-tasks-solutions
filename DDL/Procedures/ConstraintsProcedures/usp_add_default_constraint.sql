use CinemaDb;
go

if object_id('dbo.usp_AddDefaultConstraint', 'P') is not null
    drop procedure dbo.usp_AddDefaultConstraint
go

create procedure dbo.usp_AddDefaultConstraint
	@tablename		nvarchar(50),
	@fieldName		nvarchar(50),
	@defaultValue	nvarchar(100),
	@constraintName nvarchar(200)
as
begin
	declare @queryText nvarchar(1000) = 
	'if object_id(''{constraintName}'',''D'') is not null
		alter table {tableName} drop constraint {constraintName}

	alter table {tableName} add constraint {constraintName} default {defaultValue} for {fieldName}
	'
	set @queryText = replace(@queryText, '{tablename}',			@tablename)
	set @queryText = replace(@queryText, '{fieldName}',			@fieldName)
	set @queryText = replace(@queryText, '{defaultValue}',		@defaultValue)
	set @queryText = replace(@queryText, '{constraintName}',	@constraintName)

	exec(@queryText);
end
go