use CinemaDb;
go

if object_id('dbo.usp_AddAlternateKeyConstraint', 'P') is not null
    drop procedure dbo.usp_AddAlternateKeyConstraint
go

create procedure dbo.usp_AddAlternateKeyConstraint
(
    @tableName         nvarchar(100),
    @fieldsName        nvarchar(150),
    @constraintName    nvarchar(100)
)
as
begin
    declare @queryText nvarchar(1000) =
    '
        if object_id(''{constraintName}'', ''UQ'') is not null
            alter table {tableName} drop constraint {constraintName}

        alter table {tableName} 
            add constraint {constraintName} unique ({fieldsName})
    '

    set @queryText = replace(@queryText,    '{tableName}',         @tableName)
    set @queryText = replace(@queryText,    '{fieldsName}',        @fieldsName)
    set @queryText = replace(@queryText,    '{constraintName}',    @constraintName)

    execute(@queryText)
end
go