use CinemaDb;
go

if object_id('dbo.usp_AddCheckConstraint','P') is not null
	drop procedure dbo.usp_AddCheckConstraint
go

create procedure dbo.usp_AddCheckConstraint
(
	@tableName		nvarchar(50),
	@conditionExp	nvarchar(100),
	@constraintName nvarchar(100)
)
as
begin
	declare @queryText nvarchar(1000) = 
	'
	if object_id (''{constraintName}'',''C'') is not null
		alter table {tableName} drop constraint {constraintName}
	
	alter table {tableName} 
		add constraint {constraintName} check ({conditionExp})
	'
	set @queryText = replace(@queryText, '{tableName}',			@tableName);
	set @queryText = replace(@queryText, '{conditionExp}',		@conditionExp);
	set @queryText = replace(@queryText, '{constraintName}',	@constraintName);

	exec (@queryText);
end
go