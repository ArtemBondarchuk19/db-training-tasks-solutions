use CinemaDb
go

if object_id('dbo.usp_AddForeignKeyConstraint', 'P') is not null
	drop procedure dbo.usp_AddForeignKeyConstraint
go

create procedure dbo.usp_AddForeignKeyConstraint
(
	@childTableName		nvarchar(100),
	@childFieldsName	nvarchar(100),
	@parentTableName	nvarchar(100),
	@parentFieldsName	nvarchar(100),
	@constraintName		nvarchar(100)
)
as
begin
	declare @queryText nvarchar(1000) =
	'
		if object_id(''{constraintName}'', ''F'') is not null
			alter table {childTableName} drop constraint {constraintName}

		alter table {childTableName} 
			add constraint {constraintName} foreign key ({childFieldsName}) references {parentTableName} ({parentFieldsName})
	'

	set @queryText = replace(@queryText,	'{childTableName}',		@childTableName)
	set @queryText = replace(@queryText,	'{childFieldsName}',	@childFieldsName)
	set @queryText = replace(@queryText,	'{parentTableName}',	@parentTableName)
	set @queryText = replace(@queryText,	'{parentFieldsName}',	@parentFieldsName)
	set @queryText = replace(@queryText,	'{constraintName}',		@constraintName)

	execute(@queryText)
end
go