use CinemaDb
go

if object_id('dbo.usp_ParseJsonMostSaledMovies', 'P') is not null
    drop procedure dbo.usp_ParseJsonMostSaledMovies
go

create proc dbo.usp_ParseJsonMostSaledMovies
(
    @json nvarchar(max)
)
as
begin
    begin try

            if  replace(replace(replace (@json,' ',''), char(9), ''), CHAR(13) + CHAR(10), '') like '{}'
            set @json =N'{
                            "startDate": null,
                            "endDate": null,
                            "countries": null
                            }'

            create table        #JsonMSM
            (
                StartDate       date,
                EndDate         date,
                Countries       nvarchar(max)
            )

            declare @currentDate date = getdate()

            insert into         #JsonMSM
            select              datefromparts(year(coalesce(js.StartDate, @currentDate)), month(coalesce(js.StartDate, @currentDate)), 1),
                                datefromparts(year(coalesce(js.EndDate, @currentDate)), month(coalesce(js.EndDate, @currentDate)), 1),
                                isnull(js.Countries, (select [Name] as [name] from dbo.Country for json auto))
            from                openjson(@json)
                                with
                                (
                                    StartDate       date            '$.startDate',
                                    EndDate         date            '$.endDate',
                                    Countries       nvarchar(max)   '$.countries' as json
                                ) js

            if object_id('tempdb..#Months') is not null
            drop table #Months

            create table        #Months
                                (
                                    [Month] date
                                )

            ;with CTE_months ([date])
            as
            (
                select          convert(date,msm.StartDate) StartDate
                from            #JsonMSM msm
                union all
                select          dateadd(month,1,CTE_months.[date])
                from            CTE_months
                cross join      #JsonMSM msm
                where           dateadd(month,1,[date]) <= msm.EndDate
            )

            insert into         #Months
            select              *
            from                CTE_months

            create table        #Countries
                                (
                                    CountryName     nvarchar(50)
                                )

            insert into         #Countries
            select              co.CountryName
            from                #JsonMSM msm
            outer apply         openjson(msm.Countries)
                                with
                                (
                                    CountryName     nvarchar(50)       '$.name'
                                ) co

            create table        #CountryMonth
            (
                [Country]       nvarchar(50),
                [Month]         date
            )

            insert into         #CountryMonth
            select              *
            from                #Countries
            cross join          #Months

            create table        #SourceTemp
            (
                Country         nvarchar(50),
                [Month]         date,
                [Movie]         nvarchar(50),
                [TicketCount]   int
            )

            ;with CTE_temp
            as 
            (
                select              #CountryMonth.Country,
                                    #CountryMonth.[Month],
                                    m.[Name] as Movie,
                                    m.EIDR,
                                    t.TicketId
                from                #CountryMonth
                join                dbo.Country c on #CountryMonth.Country = c.[Name]
                join                dbo.MovieCountryBridge bridge on c.CountryId = bridge.CountryId
                join                dbo.Movie m on bridge.MovieId = m.MovieId
                join                dbo.Screening sc on m.MovieId = sc.MovieId
                join                dbo.Ticket t on t.ScreeningId = sc.ScreeningId
                join                dbo.[Order] o on t.OrderId = o.OrderId
                join                dbo.Payment p on o.PaymentId = p.PaymentId
                where               year(sc.StartDate) = year(#CountryMonth.[Month])
                                    and month(sc.StartDate) = month(#CountryMonth.[Month])
                                    and p.IsCompleted = 1
            ),
            CTE_groups
            as
            (
                select              CTE_temp.Country,
                                    CTE_temp.[Month],
                                    CTE_temp.Movie,
                                    count(*) as TicketCount
                from                CTE_temp
                group by            CTE_temp.Country,
                                    CTE_temp.Movie,
                                    CTE_temp.EIDR,
                                    CTE_temp.[Month]
            )
            insert into         #SourceTemp
            select              cm.Country,
                                cm.[Month],
                                gr.Movie,
                                gr.TicketCount
            from                #CountryMonth cm
            left join           CTE_groups gr on cm.Country = gr.Country
                                and cm.[Month] = gr.[Month]

            if object_id('tempdb..#Source') is not null
            drop table #Source

            create table    #Source
            (
                Country         varchar(50),
                [Month]         date,
                Movie           varchar(50),
                TicketCount     int
            )

            ;with CTE_MaxPerMonth
            as
            (
                select          st.Country,
                                st.[Month],
                                max(st.TicketCount) as MaxTicketCount
                from            #SourceTemp st
                group by        st.Country,
                                st.[Month]
            )
            insert into     #Source
            select          st.Country,
                            st.[Month],
                            st.Movie,
                            st.TicketCount
            from            #SourceTemp st
            left join       CTE_MaxPerMonth m on st.Country = m.Country
                            and st.[Month] = m.[Month]
                            and st.TicketCount = m.MaxTicketCount
            where           st.Movie is null
                            or st.Movie is not null and m.MaxTicketCount is not null

            ;with CTE_delete as
            (
                select      s.Country,
                            s.[Month],
                            s.Movie,
                            ROW_NUMBER() OVER(PARTITION BY [Country], [Month] ORDER BY [Country], [Month]) RN
                from        #SourceTemp s
            )
            delete #Source from 
            (
                select          *
                from            CTE_delete
                where           RN > 1
            ) as deleted
            where           #Source.Country = deleted.Country
                            and #Source.[Month] = deleted.[Month]
                            and #Source.Movie = deleted.Movie

            create table    #MaxForPeriod
            (
                Country         varchar(50),
                Movie           varchar(50),
                TicketCount     int
            )

            ;with CTE_period
            as
            (
                select          Country,
                                max(s.TicketCount) as MaxForPeriod
                from            #Source s
                group by        s.Country
            )
            insert into     #MaxForPeriod
            select          CTE_period.Country,
                            #Source.Movie,
                            #Source.TicketCount
            from            CTE_period
            left join       #Source on #Source.TicketCount is not null
                            and #Source.Country = CTE_period.Country
                            and #Source.TicketCount = CTE_period.MaxForPeriod

            ;with CTE_delete2 as
            (
                select      m.Country,
                            m.Movie,
                            m.TicketCount,
                            ROW_NUMBER() OVER(PARTITION BY m.[Country] ORDER BY m.[Country]) RN
                from        #MaxForPeriod m
            )
            delete #MaxForPeriod from
            (
                select          *
                from            CTE_delete2
                where           RN > 1
            ) as deleted
            where           #MaxForPeriod.Country = deleted.Country
                            and #MaxForPeriod.Movie = deleted.Movie

            declare @cols nvarchar(max)
            select      @cols = string_agg('[' + concat(datename(month, [Month]), ' ', year([Month])) + ']', ', ')
            from        #Months m

            declare @query nvarchar(max)

            set @query = 'SELECT Country,' + @cols + ', BestSeller from 
                            (
                                select          st.Country,
                                                concat(datename(month, st.[Month]),'' '', year(st.[Month])) as [Month],
                                                iif(st.Movie is null, null, concat(st.Movie,'' / '', st.TicketCount)) as Movie,
                                                iif(m.Movie is null, null, concat(m.Movie,'' / '', m.TicketCount)) as BestSeller
                                from            #Source st
                                join            #MaxForPeriod m on st.Country = m.Country
                            ) x
                            pivot
                            (
                                max(x.Movie)
                                for Month in (' + @cols+ ')
                            ) p'

            exec(@query);

    end try
    begin catch
        print 'MostSaledMovies procedure finished with an error'
    end catch
end
go