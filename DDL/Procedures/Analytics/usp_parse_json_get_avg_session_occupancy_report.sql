﻿use CinemaDb
go

if object_id('dbo.usp_ParseJsonGetAvgSessionOccupancyReport', 'P') is not null
    drop procedure dbo.usp_ParseJsonGetAvgSessionOccupancyReport
go

create procedure dbo.usp_ParseJsonGetAvgSessionOccupancyReport
(
    @json nvarchar(max)
)
as
begin
    if replace(replace(replace(@json,char(13),''),char(10),''),' ','') = '{}'
        set @json = 
        '
            {
              "startDate": "1970-01-01 00:00:00"
            }
        '

    create table #JsonParams
    (
        StartDate       datetime,
        EndDate         datetime,
        StartTime       time,
        EndTime         time,
        StepTime        time,
        Cities          nvarchar(max),
        Countries       nvarchar(max)
    )

    insert into     #JsonParams
                    (
                        StartDate,
                        EndDate,
                        StartTime,
                        EndTime,
                        StepTime,
                        Cities,
                        Countries
                    )
    select  isnull
            (
                jsp.StartDate,   (
                                        select min(s.StartDate)
                                        from dbo.Screening s
                                 )
            ),
            isnull
            (
                jsp.EndDate,     (
                                        select max(s.EndDate)
                                        from dbo.Screening s
                                 )
            ),
            isnull(jsp.StartTime, '00:00'),
            isnull(jsp.EndTime, '23:59'),
            isnull(jsp.StepTime, '02:00'),
            jsp.Cities,
            jsp.Countries
    from openjson (@json)
    with
    (
        StartDate       datetime            '$.startDate',
        EndDate         datetime            '$.endDate',
        StartTime       time                '$.interval.start',
        EndTime         time                '$.interval.end',
        StepTime        time                '$.interval.step',
        Cities          nvarchar(max)       '$.cities'                  as json,
        Countries       nvarchar(max)       '$.countries'               as json
    ) jsp

    create table #Intervals
    (
        StartInt    time,
        EndInt      time
    )

    ;with   cte_Intervals
            (
                StartInt,
                EndInt,
                StepInt,
                LastInt
            )
    as
    (
        select  StartInt =  jsp.StartTime,
                EndInt =    cast
                            (
                                (   cast(jsp.StartTime as datetime) +
                                    cast(jsp.StepTime as datetime) -
                                    cast('00:01:00' as datetime)
                                ) as time
                            ),
                StepInt = jsp.StepTime,
                LastInt = jsp.EndTime
        from #JsonParams jsp
        union all
        select  StartInt =  cast
                            (
                                (   cast(EndInt as datetime) +
                                    cast('00:01:00' as datetime)
                                 ) as time
                            ),
                EndInt =    cast
                            (
                                (   cast(EndInt as datetime) +
                                    cast(StepInt as datetime)
                                ) as time
                            ),
                StepInt,
                LastInt
        from cte_Intervals
        where EndInt < LastInt and StartInt < EndInt
    )

    insert into     #Intervals
                    (
                        StartInt,
                        EndInt
                    )
    select  cte.StartInt,
            cte.EndInt
    from cte_Intervals cte

    create table #ScreeningsTimes
    (
        ScreeningId         uniqueidentifier,
        Interval            nvarchar(11)
    )

    insert into     #ScreeningsTimes
                    (
                        ScreeningId,
                        Interval
                    )
    select  s.ScreeningId,
            concat(convert(nvarchar(5), cte.StartInt), '-', convert(nvarchar(5), cte.EndInt))
    from dbo.Screening s
    cross apply #Intervals cte
    where   cast(s.StartDate as time) <= cte.EndInt and
            cast(s.EndDate as time) >= cte.StartInt

    create table #CitySource
    (
        CityId      uniqueidentifier
    )

    insert into     #CitySource
                    (
                        CityId
                    )
    select ct.CityId
    from #JsonParams jsp
    cross apply openjson(jsp.Cities)
    with
    (
        CityName        nvarchar(50)        '$.name',
        CountryName     nvarchar(50)        '$.country.name'
    ) jspc
    join dbo.Country ctr    on      jspc.CountryName = ctr.[Name]
    join dbo.City ct        on      ct.[Name] = jspc.CityName and
                                    ct.CountryId = ctr.CountryId

    if not exists
    (
        select 1
        from #CitySource
    )
    begin
        insert into     #CitySource
                        (
                            CityId
                        )
        select ct.CityId
        from #JsonParams jsp
        cross apply openjson(jsp.Countries)
        with
        (
            CountryName     nvarchar(50)        '$.name'
        ) jspc
        join dbo.Country ctr    on      jspc.CountryName = ctr.[Name]
        join dbo.City ct        on      ctr.CountryId = ct.CountryId

        if not exists
        (
            select 1
            from #CitySource
        )
        begin
            insert into     #CitySource
                            (
                                CityId
                            )
            select ct.CityId
            from dbo.City ct
        end
    end

    create table #AttendedScreenings
    (
        ScreeningId     uniqueidentifier,
        CityId          uniqueidentifier,
        TicketNumber    int
    )

    insert into     #AttendedScreenings
                    (
                        ScreeningId,
                        CityId,
                        TicketNumber
                    )
    select  s.ScreeningId,
            ct.CityId,
            count(t.TicketId)
    from dbo.Ticket t
    join        dbo.Screening s    on      t.ScreeningId = s.ScreeningId
    join        dbo.[Order] o      on      o.OrderId = t.OrderId
    join        dbo.Payment p      on      p.PaymentId = o.PaymentId
    join        dbo.Hall h         on      h.HallId = s.HallId
    join        dbo.Cinema c       on      c.CinemaId = h.CinemaId
    join        #CitySource ct     on      ct.CityId = c.CityId
    cross join  #JsonParams jsp
    where   s.StartDate >= jsp.StartDate and
            s.EndDate <= jsp.EndDate and
            p.IsCompleted = 1
    group by    s.ScreeningId,
                ct.CityId

    create table #AllScreenings
    (
        ScreeningId     uniqueidentifier,
        CityId          uniqueidentifier,
        TicketNumber    int
    )

    insert into     #AllScreenings
                    (
                        ScreeningId,
                        CityId,
                        TicketNumber
                    )
    select  s.ScreeningId,
            ct.CityId,
            count(t.TicketId)
    from #CitySource ct
    join        dbo.Cinema c        on      c.CityId = ct.CityId
    join        dbo.Hall h          on      h.CinemaId = c.CinemaId
    join        dbo.Screening s     on      s.HallId = h.HallId
    join        dbo.Ticket t        on      t.ScreeningId = s.ScreeningId
    cross join  #JsonParams jsp
    where   s.StartDate >= jsp.StartDate and
            s.EndDate <= jsp.EndDate
    group by    s.ScreeningId,
                ct.CityId

    declare @intervals nvarchar(max)
    select @intervals = string_agg('[' + convert(nvarchar(5), cte.StartInt) + '-' + convert(nvarchar(5), cte.EndInt) + ']', ', ')
    from #Intervals cte

    declare @queryText nvarchar(max) =
    '
        select *
        from
        (
            select  st.Interval interval,
                    convert(nvarchar(10), round((convert(float, ats.TicketNumber) / als.TicketNumber), 4) * 100) + ''%'' percents,
                    concat(ct.[Name], '' / '', ctr.[Name]) ''City / Country''
            from #JsonParams jsp
            cross join #CitySource cs
            join        dbo.City ct                    on      ct.CityId = cs.CityId
            join        dbo.Country ctr                on      ctr.CountryId = ct.CountryId
            join        dbo.Cinema c                   on      c.CityId = ct.CityId
            join        dbo.Hall h                     on      h.CinemaId = c.CinemaId
            join        dbo.Screening s                on      s.HallId = h.HallId
            join        dbo.Ticket t                   on      t.ScreeningId = s.ScreeningId
            left join   #ScreeningsTimes st            on      st.ScreeningId = s.ScreeningId
            left join   #AttendedScreenings ats        on      ats.ScreeningId = st.ScreeningId and
                                                               ats.CityId = ct.CityId
            join        #AllScreenings als             on      als.ScreeningId = st.ScreeningId and
                                                               als.CityId = ct.CityId
            where   s.StartDate >= jsp.StartDate and
                    s.EndDate <= jsp.EndDate
        ) t
        pivot
        (
            max(percents)
            for interval in (' + @intervals + ')
        ) pvt
    '

    exec(@queryText)
end
go