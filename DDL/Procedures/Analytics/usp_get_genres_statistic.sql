use CinemaDb;
go
if object_id('dbo.usp_GetGenresStatistic', 'P') is not null
    drop procedure dbo.usp_GetGenresStatistic
go

create procedure dbo.usp_GetGenresStatistic
(
    @json nvarchar(max)
)
as
begin
    create table #JsonParams
    (
        StartDate   int,
        EndDate     int,
        Genres      nvarchar(max),
        LowerBound  decimal(15,2)
    )
    insert into         #JsonParams
    select              year(isnull(jp.StartDate,(select min(o.[Date]) from dbo.[Order] o where o.PaymentId is not null))),
                        year(isnull(jp.EndDate,(select max(o.[Date]) from dbo.[Order] o where o.PaymentId is not null))),
                        jp.Genres,
                        isnull(jp.LowerBound, 0)
    from                openjson(@json)
                        with
                        (
                            StartDate   datetime            '$.startDate',
                            EndDate     datetime            '$.endDate',
                            Genres      nvarchar(max)       '$.genres'      as json,
                            LowerBound  decimal(15,2)       '$.lowerBound'
                        )  jp

    create table #Years
    (
        [Year]  int
    )

    ;with CTE_Years as
    (
        select      jp.StartDate   as [year]
        from        #JsonParams jp
        union       all 
        select      cte.[year] + 1 as [year]
        from        CTE_Years cte
        cross join  #JsonParams jp
        where      cte.[year] + 1 <= jp.EndDate
    )

    insert into #Years
    (
        [Year]
    )
    select cte.[Year]
    from CTE_Years cte

    declare @years nvarchar(max)
    select @years = string_agg('[' + convert(varchar(6), y.[Year]) + ']', ', ')
    from #Years y

    create table #Genres
    (
        GenreId uniqueidentifier
    )

    ;with CTE_Genres(GenreId)
    as
    (
        select              g.GenreId
        from                #JsonParams jp
        outer apply         openjson(jp.Genres)
                            with
                            (
                                [Name] varchar(20)  '$.name'
                            )jpg
        join                dbo.[Genre] g   on (g.[Name] = jpg.[Name] or jpg.[Name] is null)
    )

    insert into #Genres
    select      g.GenreId
    from        dbo.[Order] o
    join        dbo.Ticket t                on     t.OrderId           =    o.OrderId
    join        dbo.Screening scrg          on     scrg.ScreeningId    =    t.ScreeningId 
    join        dbo.Hall h                  on     h.HallId            =    scrg.HallId
    join        dbo.Cinema c                on     h.CinemaId          =    c.CinemaId
    join        dbo.City ct                 on     ct.CityId           =    c.CityId
    join        dbo.Country ctr             on     ctr.CountryId       =    ct.CountryId
    join        dbo.Movie  m                on     m.MovieId           =    scrg.MovieId
    join        dbo.MovieGenreBridge mgb    on     mgb.MovieId         =    m.MovieId
    join        dbo.Genre g                 on     g.GenreId           =    mgb.GenreId
    join        CTE_Genres cte              on     cte.GenreId          =   g.GenreId
    join        dbo.Payment p               on     p.PaymentId         =    o.PaymentId
    cross join  #JsonParams jp
    where       year(o.[Date]) between jp.StartDate and jp.EndDate
    group by g.GenreId, jp.LowerBound
    having sum(t.FinalPrice) > jp.LowerBound

    create table #Source
    (
        Genre      varchar(20),
        [Year]     int,
        Country    nvarchar(50),
        Income     decimal(15,2)
    )

    ;with CTE_Source
    as
    (
    select      g.[Name] as genre,
                year(o.[Date]) as years,
                ctr.[Name] as country,
                sum(t.FinalPrice) as sales,
                ROW_NUMBER() OVER 
                                    (
                                        PARTITION BY g.[Name],year(o.[Date]) ORDER BY sum(t.FinalPrice) DESC
                                    ) as [rank]
    from        dbo.[Order] o
    join        dbo.Ticket t                on     t.OrderId           =    o.OrderId
    join        dbo.Screening scrg          on     scrg.ScreeningId    =    t.ScreeningId 
    join        dbo.Hall h                  on     h.HallId            =    scrg.HallId
    join        dbo.Cinema c                on     h.CinemaId          =    c.CinemaId
    join        dbo.City ct                 on     ct.CityId           =    c.CityId
    join        dbo.Country ctr             on     ctr.CountryId       =    ct.CountryId
    join        dbo.Movie  m                on     m.MovieId           =    scrg.MovieId
    join        dbo.MovieGenreBridge mgb    on     mgb.MovieId         =    m.MovieId
    join        #Genres tg                  on     tg.GenreId          =    mgb.GenreId
    join        dbo.Genre g                 on     g.GenreId           =    tg.GenreId
    join        dbo.Payment p               on     p.PaymentId         =    o.PaymentId
    cross join  #JsonParams jp
    where       year(o.[Date]) between jp.StartDate and jp.EndDate
    group by    g.[Name], year(o.[Date]), ctr.[Name]
    )

    insert into #Source
    select      cte.genre,
                cte.years,
                cte.country,
                cte.sales
    from        CTE_Source cte 
    where      cte.[rank] = 1 

    declare @pivotQuery nvarchar(max) =
    N'select    *
    from        (
                    select      s.Genre as genre,
                                s.[Year],
                                concat(s.Country,''/'',s.Income) as Income
                    from        #Source s
                ) as res
    pivot
    (
        max(Income)
        for [Year] in ('+@years+')
    ) as pvt
    '
    exec (@pivotQuery)
end
go