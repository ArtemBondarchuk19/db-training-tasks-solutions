use CinemaDb
go

if object_id('dbo.usp_ParseJsonReviewScreenings', 'P') is not null
    drop procedure dbo.usp_ParseJsonReviewScreenings
go

create procedure dbo.usp_ParseJsonReviewScreenings
(
    @json nvarchar(max)
)
as
begin
    select  c3.[Name]           CountryName,
            c2.[Name]           CityName,
            c1.[Name]           CinemaName,
            h.[Name]            HallName,
            m.[Name]            MovieName,
            s.StartDate,
            s.EndDate,
            count(p.PlaceId)    FreePlaceNumber
    from openjson(@json)
    with
    (
        MovieEIDR       nvarchar(34)        '$.movie.movieEIDR',
        DateFrom        smalldatetime       '$.dateFrom',
        DateTo          smalldatetime       '$.dateTo'
    ) js
    join dbo.Movie m        on      m.EIDR = js.MovieEIDR
    join dbo.Screening s    on      s.MovieId = m.MovieId
    join dbo.Hall h         on      h.HallId = s.HallId
    join dbo.Cinema c1      on      c1.CinemaId = h.CinemaId
    join dbo.City c2        on      c2.CityId = c1.CityId
    join dbo.Country c3     on      c3.CountryId = c2.CountryId
    join dbo.Ticket t       on      t.ScreeningId = s.ScreeningId
    join dbo.Place p        on      p.PlaceId = t.PlaceId
    where   t.OrderId is null and
            js.DateFrom <= s.StartDate and
            js.DateTo >= s.EndDate
    group by    c3.[Name],
                c2.[Name],
                c1.[Name],
                h.[Name],
                m.[Name],
                s.StartDate,
                s.EndDate
end
go