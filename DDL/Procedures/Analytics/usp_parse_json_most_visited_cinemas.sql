use CinemaDb
go

if object_id('dbo.usp_ParseJsonMostVisitedCinemas', 'P') is not null
    drop procedure dbo.usp_ParseJsonMostVisitedCinemas
go

create proc dbo.usp_ParseJsonMostVisitedCinemas
(
    @json nvarchar(max)
)
as
begin
    begin try
            create table        #JsonMVC
            (
                CityName            nvarchar(50),
                CountryName         nvarchar(50),
                [Count]             int
            )

            insert into         #JsonMVC
                                (
                                    CityName,
                                    CountryName,
                                    [Count]
                                )
            select              *
            from                openjson(@json)
                                with
                                (
                                    CityName            nvarchar(50)        '$.city.name',
                                    CountryName         nvarchar(50)        '$.city.country.name',
                                    [Count]             int                 '$.count'
                                )

            select top(select [Count] from #JsonMVC)        dbo.Cinema.[Name] as CinemaName,
                                                            dbo.Cinema.[Address],
                                                            count(*) as TicketCount
            from                #JsonMVC
            join                dbo.Country on #JsonMVC.CountryName = dbo.Country.[Name]
            join                dbo.City c on #JsonMVC.CityName = c.[Name] 
                                and dbo.Country.CountryId = c.CountryId
            join                dbo.Cinema on c.CityId = dbo.Cinema.CityId
            join                dbo.Hall on dbo.Cinema.CinemaId = dbo.Hall.CinemaId
            join                dbo.Place p on dbo.Hall.HallId = p.HallId
            join                dbo.Ticket t on p.PlaceId = t.PlaceId
            join                dbo.[Order] o on t.OrderId = o.OrderId
            join                dbo.Payment pay on o.PaymentId = pay.PaymentId
            where               pay.IsCompleted = 1
            group by            dbo.Cinema.[Name],
                                dbo.Cinema.[Address]
            order by            TicketCount desc

	end try
	begin catch
		print 'MostVisitedCinemas procedure finished with an error'
	end catch
end
go