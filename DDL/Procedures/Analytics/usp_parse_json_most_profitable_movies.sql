use CinemaDb
go

if object_id('dbo.usp_ParseJsonMostProfitableMovies', 'P') is not null
    drop procedure dbo.usp_ParseJsonMostProfitableMovies
go

create proc dbo.usp_ParseJsonMostProfitableMovies
(
    @json nvarchar(max)
)
as
begin
    begin try
            create table        #JsonMPM
            (
                CinemaAddress       nvarchar(100),
                CinemaName          nvarchar(50),
                CityName            nvarchar(50),
                CountryName         nvarchar(50),
                DateFrom            datetime,
                DateTo              datetime
            )

            insert into         #JsonMPM
            select              *
            from                openjson(@json)
                                with
                                (
                                    CinemaAddress       nvarchar(100)       '$.cinema.address',
                                    CinemaName          nvarchar(50)        '$.cinema.name',
                                    CityName            nvarchar(50)        '$.cinema.city.name',
                                    CountryName         nvarchar(50)        '$.cinema.city.country.name',
                                    DateFrom            datetime            '$.dateFrom',
                                    DateTo              datetime            '$.dateTo'
                                )

            select              mv.[Name] as Movie,
                                sum(t.FinalPrice) as Profit
            from                #JsonMPM
            join                dbo.Country on #JsonMPM.CountryName = dbo.Country.[Name]
            join                dbo.City c on #JsonMPM.CityName = c.[Name] 
                                and dbo.Country.CountryId = c.CountryId
            join                dbo.Cinema on c.CityId = dbo.Cinema.CityId
                                and #JsonMPM.CinemaName = dbo.Cinema.[Name]
                                and #JsonMPM.CinemaAddress = dbo.Cinema.[Address]
            join                dbo.Hall on dbo.Cinema.CinemaId = dbo.Hall.CinemaId
            join                dbo.Place p on dbo.Hall.HallId = p.HallId
            join                dbo.Ticket t on p.PlaceId = t.PlaceId
            join                dbo.[Order] o on t.OrderId = o.OrderId
            join                dbo.Payment on o.PaymentId = dbo.Payment.PaymentId
                                and dbo.Payment.IsCompleted = 1
            join                dbo.Screening sc on t.ScreeningId = sc.ScreeningId
                                and sc.StartDate >= #JsonMPM.DateFrom
                                and sc.EndDate <= #JsonMPM.DateTo
            join                dbo.Movie mv on sc.MovieId = mv.MovieId
            group by            mv.EIDR,
                                mv.[Name]
            order by            sum(t.FinalPrice) desc
               
	end try
	begin catch
		print 'MostProfitableMovies procedure finished with an error'
	end catch
end
go