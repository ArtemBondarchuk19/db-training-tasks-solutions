set nocount on
go

print 'creating database'
if exists (select 1 from SYS.DATABASES where name = 'CinemaDb')
drop database CinemaDb
go
create database CinemaDb
go

:setvar ScriptPath "D:\Database training\db-training-tasks-solutions\"

:On Error exit

------------TABLES-------------------
:r $(ScriptPath)"DDL\Tables\"actor.sql
print 'actor created'
:r $(ScriptPath)"DDL\Tables\"cinema.sql
print 'cinema created'
:r $(ScriptPath)"DDL\Tables\"city.sql
print 'city created'
:r $(ScriptPath)"DDL\Tables\"country.sql
print 'country created'
:r $(ScriptPath)"DDL\Tables\"display_type.sql
print 'display_type created'
:r $(ScriptPath)"DDL\Tables\"genre.sql
print 'genre created'
:r $(ScriptPath)"DDL\Tables\"hall.sql
print 'hall created'
:r $(ScriptPath)"DDL\Tables\"movie.sql
print 'movie created'
:r $(ScriptPath)"DDL\Tables\"movie_actor_bridge.sql
print 'movie_actor_bridge created'
:r $(ScriptPath)"DDL\Tables\"movie_country_bridge.sql
print 'movie_country_bridge created'
:r $(ScriptPath)"DDL\Tables\"movie_genre_bridge.sql
print 'movie_genre_bridge created'
:r $(ScriptPath)"DDL\Tables\"order.sql
print 'order created'
:r $(ScriptPath)"DDL\Tables\"payment.sql
print 'payment created'
:r $(ScriptPath)"DDL\Tables\"payment_type.sql
print 'payment_type created'
:r $(ScriptPath)"DDL\Tables\"place.sql
print 'place created'
:r $(ScriptPath)"DDL\Tables\"place_type.sql
print 'place_type created'
:r $(ScriptPath)"DDL\Tables\"role.sql
print 'role created'
:r $(ScriptPath)"DDL\Tables\"role_history.sql
print 'role_history created'
:r $(ScriptPath)"DDL\Tables\"screening.sql
print 'screening created'
:r $(ScriptPath)"DDL\Tables\"screening_type.sql
print 'screening_type created'
:r $(ScriptPath)"DDL\Tables\"ticket.sql
print 'ticket created'
:r $(ScriptPath)"DDL\Tables\"user.sql
print 'user created'

:r $(ScriptPath)"DDL\Tables\Jsons\"cinema_json.sql
print 'cinema_json'
:r $(ScriptPath)"DDL\Tables\Jsons\"hall_json.sql
print 'hall_json'
:r $(ScriptPath)"DDL\Tables\Jsons\"place_json.sql
print 'place_json'
:r $(ScriptPath)"DDL\Tables\Jsons\"movie_json.sql
print 'movie_json created'
:r $(ScriptPath)"DDL\Tables\Jsons\"register_json.sql
print 'register_json created'
:r $(ScriptPath)"DDL\Tables\Jsons\"make_order_json.sql
print 'make_order_json created'
:r $(ScriptPath)"DDL\Tables\Jsons\"screening_json.sql
print 'screening_json created'

------------FUNTIONS----------------------
:r $(ScriptPath)"DDL\Functions\"ufn_Is_user_admin.sql
PRINT 'ufn_Is_user_admin'
:r $(ScriptPath)"DDL\Functions\"ufn_get_default_user_role_id.sql
PRINT 'ufn_get_default_user_role_id'
:r $(ScriptPath)"DDL\Functions\"ufn_get_screening_type_id.sql
PRINT 'ufn_get_screening_type_id'
:r $(ScriptPath)"DDL\Functions\"ufn_is_valid_email.sql
PRINT 'ufn_get_screening_type_id'
:r $(ScriptPath)"DDL\Functions\"ufn_get_capitals.sql
PRINT 'ufn_get_capitals'
:r $(ScriptPath)"DDL\Functions\"ufn_rand_normal.sql
PRINT 'ufn_rand_normal'
:r $(ScriptPath)"DDL\Functions\"ufn_rand_normal_decimal.sql
PRINT 'ufn_rand_normal_decimal'

-- Artem's functions
:r $(ScriptPath)"DDL\Functions\"ufn_get_random_alphanumericstring.sql
PRINT 'ufn_get_random_alphanumericstring'
-- Artem's view for random values
:r $(ScriptPath)"DDL\Views\"random.sql
PRINT 'random'

:r $(ScriptPath)"DDL\Functions\"ufn_get_random_number.sql
PRINT 'ufn_get_random_number'
:r $(ScriptPath)"DDL\Functions\"ufn_get_random_value.sql
PRINT 'ufn_get_random_value.sql'

------------CONSTRAINTS-------------------
:r $(ScriptPath)"DDL\Procedures\ConstraintsProcedures\"usp_add_alternate_key_constraint.sql
PRINT 'usp_add_alternate_key_constraint'
:r $(ScriptPath)"DDL\Procedures\ConstraintsProcedures\"usp_add_check_constraint.sql
PRINT 'usp_add_check_constraint'
:r $(ScriptPath)"DDL\Procedures\ConstraintsProcedures\"usp_add_default_constraint.sql
PRINT 'usp_add_default_constraint'
:r $(ScriptPath)"DDL\Procedures\ConstraintsProcedures\"usp_add_foreign_key_constraint.sql
PRINT 'usp_add_foreign_key_constraint'

-----------CONSTRAINTS USING----------------
:r $(ScriptPath)"DDL\Constraints\"constraint_actor.sql
PRINT 'constraint_actor'
:r $(ScriptPath)"DDL\Constraints\"constraint_cinema.sql
PRINT 'constraint_cinema'
:r $(ScriptPath)"DDL\Constraints\"constraint_city.sql
PRINT 'constraint_city'
:r $(ScriptPath)"DDL\Constraints\"constraint_country.sql
PRINT 'constraint_country'
:r $(ScriptPath)"DDL\Constraints\"constraint_display_type.sql
PRINT 'constraint_display_type'
:r $(ScriptPath)"DDL\Constraints\"constraint_payment_type.sql
PRINT 'constraint_payment_type'
:r $(ScriptPath)"DDL\Constraints\"constraint_genre.sql
PRINT 'constraint_genre'
:r $(ScriptPath)"DDL\Constraints\"constraint_hall.sql
PRINT 'constraint_hall'
:r $(ScriptPath)"DDL\Constraints\"constraint_movie.sql
PRINT 'constraint_movie'
:r $(ScriptPath)"DDL\Constraints\"constraint_movie_actor_bridge.sql
PRINT 'constraint_movie_actor_bridge'
:r $(ScriptPath)"DDL\Constraints\"constraint_movie_country_bridge.sql
PRINT 'constraint_movie_country_bridge'
:r $(ScriptPath)"DDL\Constraints\"constraint_movie_genre_bridge.sql
PRINT 'constraint_movie_genre_bridge'
:r $(ScriptPath)"DDL\Constraints\"constraint_screening.sql
PRINT 'constraint_screening'
:r $(ScriptPath)"DDL\Constraints\"constraint_order.sql
PRINT 'constraint_order'
:r $(ScriptPath)"DDL\Constraints\"constraint_payment.sql
PRINT 'constraint_payment'
:r $(ScriptPath)"DDL\Constraints\"constraint_place.sql
PRINT 'constraint_place'
:r $(ScriptPath)"DDL\Constraints\"constraint_place_type.sql
PRINT 'constraint_place_type'
:r $(ScriptPath)"DDL\Constraints\"constraint_role.sql
PRINT 'constraint_role'
:r $(ScriptPath)"DDL\Constraints\"constraint_role_history.sql
PRINT 'constraint_role_history'
:r $(ScriptPath)"DDL\Constraints\"constraint_screening_type.sql
PRINT 'constraint_screening_type'
:r $(ScriptPath)"DDL\Constraints\"constraint_ticket.sql
PRINT 'constraint_ticket'
:r $(ScriptPath)"DDL\Constraints\"constraint_user.sql
PRINT 'constraint_user'

-------------------PARSERS-------------------------------
:r $(ScriptPath)"DDL\Procedures\Parsers\"usp_parse_json_add_country.sql
PRINT 'usp_parse_json_add_country'
:r $(ScriptPath)"DDL\Procedures\Parsers\"usp_parse_json_add_display_type.sql
PRINT 'usp_parse_json_add_display_type'
:r $(ScriptPath)"DDL\Procedures\Parsers\"usp_parse_json_add_genre.sql
PRINT 'usp_parse_json_add_genre'
:r $(ScriptPath)"DDL\Procedures\Parsers\"usp_parse_json_add_place_type.sql
PRINT 'usp_parse_json_add_place_type'
:r $(ScriptPath)"DDL\Procedures\Parsers\"usp_parse_json_add_role.sql
PRINT 'usp_parse_json_add_role'
:r $(ScriptPath)"DDL\Procedures\Parsers\"usp_parse_json_add_screening_type.sql
PRINT 'usp_parse_json_add_screening_type'
:r $(ScriptPath)"DDL\Procedures\Parsers\"usp_parse_json_add_payment_type.sql
PRINT 'usp_parse_json_add_payment_type'
:r $(ScriptPath)"DDL\Procedures\Parsers\"usp_parse_json_add_city.sql
PRINT 'usp_parse_json_add_city'
:r $(ScriptPath)"DDL\Procedures\Parsers\"usp_parse_json_add_cinema.sql
PRINT 'usp_parse_json_add_cinema'
:r $(ScriptPath)"DDL\Procedures\Parsers\"usp_parse_json_add_hall.sql
PRINT 'usp_parse_json_add_hall'
:r $(ScriptPath)"DDL\Procedures\Parsers\"usp_parse_json_add_place.sql
PRINT 'usp_parse_json_add_place'
:r $(ScriptPath)"DDL\Procedures\Parsers\"usp_parse_json_add_movie.sql
PRINT 'usp_parse_json_add_movie'
:r $(ScriptPath)"DDL\Procedures\Parsers\"usp_parse_json_add_screening.sql
PRINT 'usp_parse_json_add_screening'
:r $(ScriptPath)"DDL\Procedures\Parsers\"usp_parse_json_change_role.sql
PRINT 'usp_parse_json_change_role'
:r $(ScriptPath)"DDL\Procedures\Parsers\"usp_parse_json_login.sql
PRINT 'usp_parse_json_login'
:r $(ScriptPath)"DDL\Procedures\Parsers\"usp_parse_json_make_order.sql
PRINT 'usp_parse_json_make_order'
:r $(ScriptPath)"DDL\Procedures\Parsers\"usp_parse_json_register.sql
PRINT 'usp_parse_json_register'

:r $(ScriptPath)"DDL\Procedures\Analytics\"usp_parse_json_most_profitable_movies.sql
PRINT 'usp_parse_json_most_profitable_movies'
:r $(ScriptPath)"DDL\Procedures\Analytics\"usp_parse_json_most_visited_cinemas.sql
PRINT 'usp_parse_json_most_visited_cinemas'
:r $(ScriptPath)"DDL\Procedures\Analytics\"usp_parse_json_review_screenings.sql
PRINT 'usp_parse_json_review_screenings'

------------TRIGGERS-------------
:r $(ScriptPath)"DDL\Triggers\"order_insert.sql
PRINT 'order_insert'

-------------GENERATORS-----------------
:r $(ScriptPath)DDL\Procedures\Generators\Common\usp_generate_random_string.sql
PRINT 'usp_generate_random_string'
:r $(ScriptPath)DDL\Procedures\Generators\Common\usp_rand_normal.sql
PRINT 'usp_rand_normal'
:r $(ScriptPath)DDL\Procedures\Generators\Common\usp_rand_normal_decimal.sql
PRINT 'usp_rand_normal_decimal'
:r $(ScriptPath)DDL\Procedures\Generators\usp_generate_user_registration.sql
PRINT 'usp_generate_movie'
:r $(ScriptPath)DDL\Procedures\Generators\usp_generate_cinema.sql
PRINT 'usp_generate_cinema'
:r $(ScriptPath)DDL\Procedures\Generators\usp_generate_hall.sql
PRINT 'usp_generate_hall'
:r $(ScriptPath)DDL\Procedures\Generators\usp_generate_place.sql
PRINT 'usp_generate_place'
:r $(ScriptPath)"DDL\Procedures\Generators\"usp_generate_movie.sql
PRINT 'usp_generate_movie'
:r $(ScriptPath)"DDL\Procedures\Generators\"usp_generate_make_order.sql
PRINT 'usp_generate_make_order'
:r $(ScriptPath)"DDL\Procedures\Generators\"usp_generate_screening.sql
PRINT 'usp_generate_screening'

-------------DML-----------------
:r $(ScriptPath)"DML\"load_placeTypes.sql
PRINT 'load_placeTypes'
:r $(ScriptPath)"DML\"load_genres.sql
PRINT 'load_genres'
:r $(ScriptPath)"DML\"load_display_types.sql
PRINT 'load_display_types'
:r $(ScriptPath)"DML\"load_countries.sql
PRINT 'load_countries'
:r $(ScriptPath)"DML\"load_cities.sql
PRINT 'load_cities'
:r $(ScriptPath)"DML\"load_role.sql
PRINT 'load_role'
:r $(ScriptPath)"DML\"load_screening_type.sql
PRINT 'load_screening_type'
:r $(ScriptPath)"DML\"load_payment_types.sql
PRINT 'load_payment_types'

:r $(ScriptPath)"DML\"load_data.sql
PRINT 'load_data'

:r $(ScriptPath)"DML\"exec_register.sql
PRINT 'load_register'
update dbo.[User]
set RoleId = 2 -- This is admin roleId. Necessary for adding cinemas, places, halls, movies and screenings